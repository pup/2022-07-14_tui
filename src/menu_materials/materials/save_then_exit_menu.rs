/*
use tui_utilities::utilities_operations::component_operations::ComponentOperations;

pub struct SaveThenExitMenu
{
    pub menu:tui_utilities::utilities::menu::Menu,
    pub cancel_notice:bool,
    pub exit_notice:bool,
    pub save_then_exit_notice:bool,
}
impl SaveThenExitMenu
{
    pub fn new_save_then_exit_menu
    (
        owner:usize,
        position:tui_utilities::utilities::position::Position,
        world:std::rc::Rc<std::cell::RefCell<bevy_ecs::world::World>>,
    )
    {
        /*What the menu looks like by default.
           6
        1115               exit?
        22 4         you did not save!
        3  3
        4  2 cancel   no save exit   save exit
           1234567891234567891234567891234567891
            12
            1234567891234567
            1234567891
            12345678912
            12345678912345678912345678
        */
        let mut borrowed_world = world.borrow_mut();
        let mut menus = borrowed_world.get_resource_mut::<Vec<std::sync::Arc<std::sync::Mutex<dyn tui_utilities::utilities_operations::menu_operations::MenuOperations>>>>().unwrap();
        let menu_position = menus.len();
        let mut component_list:Vec<tui_utilities::utilities::component::Component> = Vec::new();
        {
            let exit_question_mark = "exit?".to_string();
            let exit_question_mark_label = crate::component_materials::materials::label::Label::new_label
            (
                Some
                (
                    exit_question_mark.clone()
                ), 
                Some
                (
                    tui_utilities::utilities::component::Component::new_component
                    (
                        None, 
                        Some(menu_position.clone()), 
                        Some(0), 
                        Some(crate::component_materials::materials::button::Button::get_type_name_button()), 
                        {
                            let mut component_position = position.clone();
                            component_position.move_right(16);
                            component_position.move_down(1);
                            Some(component_position)
                        }, 
                        Some
                        (
                            tui_utilities::utilities::bounds::Bounds::new_bounds
                            (
                                exit_question_mark.len(),
                                1
                            )
                        )
                    )
                )
            );
            let mut quarantined_borrowed_world = world.borrow_mut();
            let exit_question_mark_label_entity = quarantined_borrowed_world
            .spawn()
            .insert(exit_question_mark_label)
            .id();
            let mut component_entity = quarantined_borrowed_world.get_entity_mut(exit_question_mark_label_entity).unwrap();
            let mut component = component_entity.get_mut::<crate::component_materials::materials::button::Button>().unwrap();
            component.set_id(Some(exit_question_mark_label_entity));
            component_list.push(component.get().clone());
        }
        {
            let you_did_not_save_exclamation_mark = "you did not save!".to_string();
            let you_did_not_save_exclamation_mark_label = crate::component_materials::materials::label::Label::new_label
            (
                Some
                (
                    you_did_not_save_exclamation_mark.clone()
                ), 
                Some
                (
                    tui_utilities::utilities::component::Component::new_component
                    (
                        None, 
                        Some(menu_position.clone()), 
                        Some(1), 
                        Some(crate::component_materials::materials::button::Button::get_type_name_button()), 
                        {
                            let mut component_position = position.clone();
                            component_position.move_right(10);
                            component_position.move_down(2);
                            Some(component_position)
                        }, 
                        Some
                        (
                            tui_utilities::utilities::bounds::Bounds::new_bounds
                            (
                                you_did_not_save_exclamation_mark.len(),
                                1
                            )
                        )
                    )
                )
            );
            let mut quarantined_borrowed_world = world.borrow_mut();
            let you_did_not_save_exclamation_mark_label_entity = quarantined_borrowed_world
            .spawn()
            .insert(you_did_not_save_exclamation_mark_label)
            .id();
            let mut component_entity = quarantined_borrowed_world.get_entity_mut(you_did_not_save_exclamation_mark_label_entity).unwrap();
            let mut component = component_entity.get_mut::<crate::component_materials::materials::button::Button>().unwrap();
            component.set_id(Some(you_did_not_save_exclamation_mark_label_entity));
            component_list.push(component.get().clone());
        }
        {
            let cancel = "cancel".to_string();
            let cancel_button = crate::component_materials::materials::button::Button::new_button
            (
                Some
                (
                    cancel.clone()
                ), 
                Some
                (
                    tui_utilities::utilities::component::Component::new_component
                    (
                        None, 
                        Some(menu_position.clone()), 
                        Some(2), 
                        Some(crate::component_materials::materials::button::Button::get_type_name_button()), 
                        {
                            let mut component_position = position.clone();
                            component_position.move_right(2);
                            component_position.move_down(4);
                            Some(component_position)
                        }, 
                        Some
                        (
                            tui_utilities::utilities::bounds::Bounds::new_bounds
                            (
                                cancel.len(),
                                1
                            )
                        )
                    )
                )
            );
            let mut quarantined_borrowed_world = world.borrow_mut();
            let cancel_button_entity = quarantined_borrowed_world
            .spawn()
            .insert(cancel_button)
            .id();
            let mut component_entity = quarantined_borrowed_world.get_entity_mut(cancel_button_entity).unwrap();
            let mut component = component_entity.get_mut::<crate::component_materials::materials::button::Button>().unwrap();
            component.set_id(Some(cancel_button_entity));
            component_list.push(component.get().clone());
        }
        {
            let no_save_exit = "no save exit".to_string();
            let no_save_exit_button = crate::component_materials::materials::button::Button::new_button
            (
                Some
                (
                    no_save_exit.clone()
                ), 
                Some
                (
                    tui_utilities::utilities::component::Component::new_component
                    (
                        None, 
                        Some(menu_position.clone()), 
                        Some(3), 
                        Some(crate::component_materials::materials::button::Button::get_type_name_button()), 
                        {
                            let mut component_position = position.clone();
                            component_position.move_right(11);
                            component_position.move_down(4);
                            Some(component_position)
                        }, 
                        Some
                        (
                            tui_utilities::utilities::bounds::Bounds::new_bounds
                            (
                                no_save_exit.len(),
                                1
                            )
                        )
                    )
                )
            );
            let mut quarantined_borrowed_world = world.borrow_mut();
            let no_save_exit_button_entity = quarantined_borrowed_world
            .spawn()
            .insert(no_save_exit_button)
            .id();
            let mut component_entity = quarantined_borrowed_world.get_entity_mut(no_save_exit_button_entity).unwrap();
            let mut component = component_entity.get_mut::<crate::component_materials::materials::button::Button>().unwrap();
            component.set_id(Some(no_save_exit_button_entity));
            component_list.push(component.get().clone());
        }
        {
            let save_exit = "save exit".to_string();
            let save_exit_button = crate::component_materials::materials::button::Button::new_button
            (
                Some
                (
                    save_exit.clone()
                ), 
                Some
                (
                    tui_utilities::utilities::component::Component::new_component
                    (
                        None, 
                        Some(menu_position.clone()), 
                        Some(4), 
                        Some(crate::component_materials::materials::button::Button::get_type_name_button()), 
                        {
                            let mut component_position = position.clone();
                            component_position.move_right(26);
                            component_position.move_down(4);
                            Some(component_position)
                        }, 
                        Some
                        (
                            tui_utilities::utilities::bounds::Bounds::new_bounds
                            (
                                save_exit.len(),
                                1
                            )
                        )
                    )
                )
            );
            let mut quarantined_borrowed_world = world.borrow_mut();
            let save_exit_button_entity = quarantined_borrowed_world
            .spawn()
            .insert(save_exit_button)
            .id();
            let mut component_entity = quarantined_borrowed_world.get_entity_mut(save_exit_button_entity).unwrap();
            let mut component = component_entity.get_mut::<crate::component_materials::materials::button::Button>().unwrap();
            component.set_id(Some(save_exit_button_entity));
            component_list.push(component.get().clone());
        }
        menus.push
        (
            std::sync::Arc::new
            (
                std::sync::Mutex::new
                (
                    SaveThenExitMenu
                    {
                        menu:tui_utilities::utilities::menu::Menu::new_menu
                        ( 
                            Some(menu_position.clone()),
                            Some(owner),
                            Some(position), 
                            {
                                Some
                                (
                                    tui_utilities::utilities::bounds::Bounds::new_bounds
                                    (
                                        28, 
                                        6
                                    )
                                )
                            }, 
                            Some
                            (
                                component_list
                            ),
                            Some
                            (
                                tui_utilities::utilities::activity::Activity::new_activity
                                (
                                    Some(false),
                                )
                            ), 
                            None
                        ),
                        cancel_notice:false,
                        exit_notice:false,
                        save_then_exit_notice:false,
                    }
                )
            )
        );
    }
    pub fn set_cancel_notice
    (
        &mut self,
        notice:bool
    )
    {
        self.cancel_notice = notice;
    }
    pub fn cancel_noticed
    (
        &self
    ) -> bool
    {
        self.cancel_notice
    }
    pub fn set_exit_notice
    (
        &mut self,
        notice:bool
    )
    {
        self.exit_notice = notice;
    }
    pub fn exit_noticed
    (
        &self
    ) -> bool
    {
        self.exit_notice
    }
    pub fn set_save_then_exit_notice
    (
        &mut self,
        notice:bool
    )
    {
        self.save_then_exit_notice = notice;
    }
    pub fn save_then_exit_noticed
    (
        &self
    ) -> bool
    {
        self.save_then_exit_notice
    }
}
*/