/*
use tui_utilities::utilities_operations::component_operations::ComponentOperations;

pub struct ExitMenu
{
  pub menu:tui_utilities::utilities::menu::Menu,
  pub cancel_notice:bool,
  pub exit_notice:bool
}
impl ExitMenu
{
    pub fn new_exit_menu
    (
      owner:usize,
      position:tui_utilities::utilities::position::Position,
      world:std::rc::Rc<std::cell::RefCell<bevy_ecs::world::World>>,
    )
    {
        /*What the menu looks like by default.
          5
        114     exit?
        2 3
        3 2 cancel  exit
          1234567891234567
           123456
           12
           1234567891
        */
        let mut borrowed_world = world.borrow_mut();
        let mut menus = borrowed_world.get_resource_mut::<Vec<std::sync::Arc<std::sync::Mutex<dyn tui_utilities::utilities_operations::menu_operations::MenuOperations>>>>().unwrap();
        let menu_position = menus.len();
        let mut component_list:Vec<tui_utilities::utilities::component::Component> = Vec::new();
        {
            let exit_question_mark = "exit?".to_string();
            let exit_question_mark_label = crate::component_materials::materials::label::Label::new_label
            (
                Some
                (
                    exit_question_mark.clone()
                ), 
                Some
                (
                    tui_utilities::utilities::component::Component::new_component
                    (
                        None, 
                        Some(menu_position.clone()), 
                        Some(0), 
                        Some(crate::component_materials::materials::button::Button::get_type_name_button()), 
                        {
                            let mut component_position = position.clone();
                            component_position.move_right(6);
                            component_position.move_down(1);
                            Some(component_position)
                        }, 
                        Some
                        (
                            tui_utilities::utilities::bounds::Bounds::new_bounds
                            (
                                exit_question_mark.len(),
                                1
                            )
                        )
                    )
                )
            );
            let mut quarantined_borrowed_world = world.borrow_mut();
            let exit_question_mark_label_entity = quarantined_borrowed_world
            .spawn()
            .insert(exit_question_mark_label)
            .id();
            let mut component_entity = quarantined_borrowed_world.get_entity_mut(exit_question_mark_label_entity).unwrap();
            let mut component = component_entity.get_mut::<crate::component_materials::materials::button::Button>().unwrap();
            component.set_id(Some(exit_question_mark_label_entity));
            component_list.push(component.get().clone());
        }
        {
            let cancel = "cancel".to_string();
            let cancel_button = crate::component_materials::materials::button::Button::new_button
            (
                Some
                (
                    cancel.clone()
                ), 
                Some
                (
                    tui_utilities::utilities::component::Component::new_component
                    (
                        None, 
                        Some(menu_position.clone()), 
                        Some(1), 
                        Some(crate::component_materials::materials::button::Button::get_type_name_button()), 
                        {
                            let mut component_position = position.clone();
                            component_position.move_right(2);
                            component_position.move_down(3);
                            Some(component_position)
                        }, 
                        Some
                        (
                            tui_utilities::utilities::bounds::Bounds::new_bounds
                            (
                                cancel.len(),
                                1
                            )
                        )
                    )
                )
            );
            let mut quarantined_borrowed_world = world.borrow_mut();
            let cancel_button_entity = quarantined_borrowed_world
            .spawn()
            .insert(cancel_button)
            .id();
            let mut component_entity = quarantined_borrowed_world.get_entity_mut(cancel_button_entity).unwrap();
            let mut component = component_entity.get_mut::<crate::component_materials::materials::button::Button>().unwrap();
            component.set_id(Some(cancel_button_entity));
            component_list.push(component.get().clone());
        }
        {
            let save_exit = "exit".to_string();
            let save_exit_button = crate::component_materials::materials::button::Button::new_button
            (
                Some
                (
                    save_exit.clone()
                ), 
                Some
                (
                    tui_utilities::utilities::component::Component::new_component
                    (
                        None, 
                        Some(menu_position.clone()), 
                        Some(2), 
                        Some(crate::component_materials::materials::button::Button::get_type_name_button()), 
                        {
                            let mut component_position = position.clone();
                            component_position.move_right(10);
                            component_position.move_down(3);
                            Some(component_position)
                        }, 
                        Some
                        (
                            tui_utilities::utilities::bounds::Bounds::new_bounds
                            (
                                save_exit.len(),
                                1
                            )
                        )
                    )
                )
            );
            let mut quarantined_borrowed_world = world.borrow_mut();
            let save_exit_button_entity = quarantined_borrowed_world
            .spawn()
            .insert(save_exit_button)
            .id();
            let mut component_entity = quarantined_borrowed_world.get_entity_mut(save_exit_button_entity).unwrap();
            let mut component = component_entity.get_mut::<crate::component_materials::materials::button::Button>().unwrap();
            component.set_id(Some(save_exit_button_entity));
            component_list.push(component.get().clone());
        }
        menus.push
        (
            std::sync::Arc::new
            (
                std::sync::Mutex::new
                (
                    ExitMenu
                    {
                        menu:
                        tui_utilities::utilities::menu::Menu::new_menu
                        ( 
                            Some(menu_position.clone()),
                            Some(owner),
                            Some(position), 
                            {

                                Some
                                (
                                    tui_utilities::utilities::bounds::Bounds::new_bounds
                                    (
                                        16, 
                                        5
                                    )
                                )
                            }, 
                            Some
                            (
                                component_list
                            ),
                            Some
                            (
                                tui_utilities::utilities::activity::Activity::new_activity
                                (
                                    Some(false),
                                )
                            ), 
                            None
                        ),
                        cancel_notice:false,
                        exit_notice:false,
                    }
                )
            )
        );
    }
    pub fn set_cancel_notice
    (
        &mut self,
        notice:bool
    )  
    {
        self.cancel_notice = notice;
    }
    pub fn cancel_noticed
    (
        &self
    ) -> bool
    {
        self.cancel_notice.clone()
    }
    pub fn set_exit_notice
    (
        &mut self,
        notice:bool
    )
    {
        self.exit_notice = notice;
    }
    pub fn exit_noticed
    (
        &self
    ) -> bool
    {
        self.exit_notice.clone()
    }
}
*/