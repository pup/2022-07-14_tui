#![allow(unused_imports)]
use thread_manager::{handle_manager_operations::HandleManagerOperations, global_list_operations::GlobalListOperations, materials_operations::MaterialsOperations, function_list_operations::FunctionListOperations, hash_key_operations::HashKeyOperations, data_space_operations::DataSpaceOperations, handle_operations::HandleOperations, global_list};
use tui_utilities::utilities_operations::{component_operations::ComponentOperations, menu_operations::MenuOperations, position_operations::PositionOperations};

use crate::component_materials::{materials_services::{button_services::ButtonServices, label_services::LabelServices}, materials_operations::label_operations::LabelOperations};

pub struct DeletionMenu
{
    pub(crate) material:std::sync::Arc<thread_manager::materials::Materials>,
    pub(crate) menu:std::sync::Arc<tui_utilities::utilities::menu::Menu>,
    pub(crate) handle:std::sync::Mutex<Option<std::sync::Arc<thread_manager::handle::Handle>>>,
    pub(crate) operator:std::sync::Mutex<Option<std::sync::Arc<thread_manager::operator::Operator>>>,
    pub(crate) function_list:std::sync::Mutex<Option<std::sync::Arc<thread_manager::function_list::FunctionList>>>,
    pub(crate) cancel_notice:std::sync::Mutex<bool>,
    pub(crate) delete_notice:std::sync::Mutex<bool>,
}
impl DeletionMenu 
{
    pub fn new_deletion_menu
    <
    T:
    thread_manager::global_list_operations::GlobalListOperations + 
    tui_utilities::utilities_materials_operations::standard_input_operations::StandardInputOperations +
    tui_utilities::utilities_materials_operations::standard_output_operations::StandardOutputOperations +
    thread_manager::handle_manager_operations::HandleManagerOperations +
    thread_manager::materials_operations::MaterialsOperations +
    Send +
    Sync +
    'static
    >
    (
        owner_material_id:usize,
        menu_position:std::sync::Arc<tui_utilities::utilities::position::Position>,
        global_list:&std::sync::Arc<T>,
    ) -> usize
    {
        /*What the menu looks like by default.
            4
          113     delete?
          2 3
          3 2 cancel   delete
            1234567891234567891
             12
             123456
             12345678912
        */
        //Setup deletion menu.
        let deletion_menu =
        std::sync::Arc::new
        (
            DeletionMenu
            {
                material:
                thread_manager::materials::Materials::new_material(),
                menu:
                tui_utilities::utilities::menu::Menu::new_menu
                ( 
                    None,
                    Some(owner_material_id.clone()),
                    Some(menu_position.clone()), 
                    {
                        Some
                        (
                            tui_utilities::utilities::bounds::Bounds::new_bounds
                            (
                                19, 
                                4
                            )
                        )
                    }, 
                    None,
                    Some
                    (
                        tui_utilities::utilities::activity::Activity::new_activity
                        (
                            Some(false),
                        )
                    ), 
                    None
                ),
                handle:std::sync::Mutex::new(None),
                operator:std::sync::Mutex::new(None),
                function_list:
                std::sync::Mutex::new(None),
                cancel_notice:
                std::sync::Mutex::new 
                (
                    false
                ),
                delete_notice:
                std::sync::Mutex::new 
                (
                    false
                ),
            }
        );
        global_list.clone().push_material(deletion_menu.clone());
        deletion_menu.clone().set_material_id_owner(Some(owner_material_id));
        //Setup delete label.
        let delete_question_mark = "delete?".to_string();
        let delete_question_mark_label = crate::component_materials::materials::label::Label::new_label
        (
            Some
            (
                delete_question_mark.clone()
            ), 
            Some
            (
                tui_utilities::utilities::component::Component::new_component
                (
                    None, 
                    deletion_menu.clone().get_material_id(), 
                    Some(0), 
                    Some(crate::component_materials::materials::label::Label::get_type_name_label()), 
                    {
                        let component_position = menu_position.clone();
                        component_position.clone().move_position_right(6);
                        component_position.clone().move_position_down(1);
                        Some(component_position)
                    }, 
                    Some
                    (
                        tui_utilities::utilities::bounds::Bounds::new_bounds
                        (
                            delete_question_mark.len(),
                            1
                        )
                    )
                )
            )
        );
        global_list.clone().push_material(delete_question_mark_label.clone());
        deletion_menu.clone().push_component(delete_question_mark_label.clone().get_component());
        //Setup delete button.
        let delete = "delete".to_string();
        let delete_button = crate::component_materials::materials::button::Button::new_button
        (
            Some
            (
                delete.clone()
            ), 
            Some
            (
                tui_utilities::utilities::component::Component::new_component
                (
                    None, 
                    deletion_menu.clone().get_material_id(), 
                    Some(1), 
                    Some(crate::component_materials::materials::button::Button::get_type_name_button()), 
                    {
                        let component_position = menu_position.clone();
                        component_position.clone().move_position_right(2);
                        component_position.clone().move_position_down(3);
                        Some(component_position)
                    }, 
                    Some
                    (
                        tui_utilities::utilities::bounds::Bounds::new_bounds
                        (
                            delete.len(),
                            1
                        )
                    )
                )
            )
        );
        global_list.clone().push_material(delete_button.clone());
        deletion_menu.clone().push_component(delete_button.clone().get_component());
        //Setup cancel button.
        let cancel = "cancel".to_string();
        let cancel_button = crate::component_materials::materials::button::Button::new_button
        (
            Some
            (
                cancel.clone()
            ), 
            Some
            (
                tui_utilities::utilities::component::Component::new_component
                (
                    None, 
                    deletion_menu.clone().get_material_id(), 
                    Some(2), 
                    Some(crate::component_materials::materials::button::Button::get_type_name_button()), 
                    {
                        let component_position = menu_position.clone();
                        component_position.clone().move_position_right(11);
                        component_position.clone().move_position_down(3);
                        Some(component_position)
                    }, 
                    Some
                    (
                        tui_utilities::utilities::bounds::Bounds::new_bounds
                        (
                            cancel.len(),
                            1
                        )
                    )
                )
            )
        );
        global_list.clone().push_material(cancel_button.clone());
        deletion_menu.clone().push_component(cancel_button.clone().get_component());
        //Set key name for deletion menu.
        let deletion_menu_handle_name = 
        format!("deletion_menu_{}{}", owner_material_id, deletion_menu.clone().get_material_id().unwrap());
        deletion_menu.clone().set_hash_key_name(Some(deletion_menu_handle_name.clone()));
        //Setup operator.
        let deletion_menu_operator = 
        thread_manager::operator::Operator::new_operator
        (
            deletion_menu_operator::<T>,
            &global_list.clone().get_global_list(),
            deletion_menu.clone().get_material_id(),
            Some(deletion_menu.clone().collect_menu_component_ids())
        );
        deletion_menu_operator.clone().insert_data(0, global_list.clone());
        //Setup the function list
        let deletion_menu_function_list =
        thread_manager::function_list::FunctionList::new_function_list
        (
            format!("{}_function_list", deletion_menu_handle_name.clone()),
            deletion_menu_handle_name.clone(), 
            deletion_menu.clone().get_material_id().unwrap(), 
            None, 
            Some
            (
                vec!
                [
                    DeletionMenu::draw_label_self,
                    DeletionMenu::on_click_button,
                    DeletionMenu::on_enter_button,
                    DeletionMenu::draw_label_self,
                    DeletionMenu::on_click_button,
                    DeletionMenu::on_enter_button,
                    DeletionMenu::draw_label_self
                ]
            ), 
            {
                let mut temp:Vec<Vec<usize>> = Vec::new();
                for index in 0..deletion_menu.clone().get_components_len()
                {
                    if index == 0 
                    {
                        temp.push
                        (
                            {
                                vec!
                                [
                                    deletion_menu.clone().get_component_from_menu(index).unwrap().get_material_id().unwrap(),
                                    global_list.clone().get_standard_input().get_vector_position().unwrap()
                                ]
                            }
                        );
                        continue;
                    }
                    temp.push
                    (
                        {
                            vec!
                            [
                                deletion_menu.clone().get_material_id().unwrap(),
                                deletion_menu.clone().get_component_from_menu(index).unwrap().get_material_id().unwrap(),
                                global_list.clone().get_standard_input().get_vector_position().unwrap()
                            ]
                        }
                    );
                    temp.push
                    (
                        {
                            vec!
                            [
                                deletion_menu.clone().get_material_id().unwrap(),
                                deletion_menu.clone().get_component_from_menu(index).unwrap().get_material_id().unwrap(),
                                global_list.clone().get_standard_input().get_vector_position().unwrap()
                            ]
                        }
                    );
                    temp.push
                    (
                        {
                            vec!
                            [
                                deletion_menu.clone().get_component_from_menu(index).unwrap().get_material_id().unwrap(),
                                global_list.clone().get_standard_input().get_vector_position().unwrap()
                            ]
                        }
                    );
                }
                Some
                (
                    temp
                )
            }
        );
        //Setup the handle.
        let deletion_menu_handle =
        thread_manager::handle::Handle::new_handle
        (
            deletion_menu_handle_name.clone(), 
            deletion_menu_operator.clone(), 
            deletion_menu_function_list.clone(), 
            global_list.clone().get_global_list(), 
            None
        );
        deletion_menu.clone().set_handle(&deletion_menu_handle);
        global_list.clone().push_handle(deletion_menu_handle.clone());
        //Push the deletion_menu to the global_list.
        global_list.clone().insert_hashed_materials(deletion_menu.clone());
        deletion_menu.clone().get_material_id().unwrap()
    }
}
/// Materials For Operator:
/// global_list
/// menu
/// delete_label
/// delete_button
/// cancel_button
pub fn deletion_menu_operator
<
T:
thread_manager::global_list_operations::GlobalListOperations + 
tui_utilities::utilities_materials_operations::standard_input_operations::StandardInputOperations +
tui_utilities::utilities_materials_operations::standard_output_operations::StandardOutputOperations +
thread_manager::handle_manager_operations::HandleManagerOperations +
thread_manager::materials_operations::MaterialsOperations +
Send +
Sync +
'static
>
(
    functions:std::sync::Arc<thread_manager::function_list::FunctionList>, 
    local_materials:Vec<std::sync::Arc<thread_manager::data_space::DataSpace>>
)
{
    loop 
    {
        for index in 0..functions.clone().get_function_list_len()
        {
            if index == 0 {continue;}
            functions.clone().get_function(index).lock().unwrap()(&local_materials.clone()[index].clone().get_data_list())
        }
        //let operators_data_list = local_materials.clone()[0].clone().get_data_list();
        //let _global_list = operators_data_list.clone()[0].clone().any_materials().downcast::<T>().unwrap();
        //let _deletion_menu = operators_data_list.clone()[1].clone().any_materials().downcast::<DeletionMenu>().unwrap();
        //let _cancel_label = operators_data_list.clone()[2].clone().any_materials().downcast::<crate::component_materials::materials::label::Label>().unwrap();
        //let _deletion_button = operators_data_list.clone()[3].clone().any_materials().downcast::<crate::component_materials::materials::button::Button>().unwrap();
        //let _cancel_button = operators_data_list.clone()[4].clone().any_materials().downcast::<crate::component_materials::materials::button::Button>().unwrap();
    }
}
