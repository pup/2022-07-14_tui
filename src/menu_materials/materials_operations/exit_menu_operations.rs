/*
use tui_utilities::utilities_operations::{menu_operations::MenuOperations, position_operations::PositionOperations, bounds_operations::BoundsOperations, activity_operations::ActivityOperations};

pub trait ExitMenuOperations
{
    fn get
    (
        &self
    ) -> &crate::menu_materials::materials::exit_menu::ExitMenu;
    fn get_mut
    (
        &mut self 
    ) -> &mut crate::menu_materials::materials::exit_menu::ExitMenu;
    fn any 
    (
        &self 
    ) -> &dyn std::any::Any;
    fn any_mut
    (
        &mut self
    ) -> &mut dyn std::any::Any;
    fn set_cancel_notice
    (
        &mut self,
        notice:bool
    )
    {
        self.get_mut().set_cancel_notice(notice)
    }
    fn cancel_noticed
    (
        &self
    ) -> bool
    {
        self.get().cancel_noticed()
    }
    fn set_exit_notice
    (
        &mut self,
        notice:bool
    )
    {
        self.get_mut().set_exit_notice(notice)
    }
    fn exit_noticed
    (
        &self
    ) -> bool
    {
        self.get().exit_noticed()
    }
}
impl ExitMenuOperations for crate::menu_materials::materials::exit_menu::ExitMenu
{
    fn get
    (
        &self
    ) -> &crate::menu_materials::materials::exit_menu::ExitMenu 
    {
        self
    }
    fn get_mut
    (
        &mut self 
    ) -> &mut crate::menu_materials::materials::exit_menu::ExitMenu 
    {
        self
    }
    fn any 
    (
        &self 
    ) -> &dyn std::any::Any 
    {
        self
    }
    fn any_mut
    (
        &mut self
    ) -> &mut dyn std::any::Any 
    {
        self
    }
}
impl tui_utilities::utilities_operations::menu_operations::MenuOperations for crate::menu_materials::materials::exit_menu::ExitMenu
{
    fn get
    (
        &self
    ) -> &tui_utilities::utilities::menu::Menu 
    {
        &self.menu
    }
    fn get_mut
    (
        &mut self
    ) -> &mut tui_utilities::utilities::menu::Menu 
    {
        &mut self.menu
    }
    fn any
    (
        &self
    ) -> &dyn std::any::Any 
    {
        self
    }
    fn any_mut
    (
        &mut self
    ) -> &mut dyn std::any::Any 
    {
        self
    }
}
impl tui_utilities::utilities_operations::position_operations::PositionOperations for crate::menu_materials::materials::exit_menu::ExitMenu
{
    fn get
    (
        &self
    ) -> &tui_utilities::utilities::position::Position 
    {
        PositionOperations::get(MenuOperations::get(self))
    }
    fn get_mut
    (
        &mut self
    ) -> &mut tui_utilities::utilities::position::Position 
    {
        PositionOperations::get_mut(MenuOperations::get_mut(self))
    }
    fn any
    (
        &self
    ) -> &dyn std::any::Any 
    {
        self
    }
    fn any_mut
    (
        &mut self
    ) -> &mut dyn std::any::Any 
    {
        self
    }
}
impl tui_utilities::utilities_operations::bounds_operations::BoundsOperations for crate::menu_materials::materials::exit_menu::ExitMenu
{
    fn get
    (
        &self
    ) -> &tui_utilities::utilities::bounds::Bounds
    {
        BoundsOperations::get(MenuOperations::get(self))
    }
    fn get_mut
    (
        &mut self
    ) -> &mut tui_utilities::utilities::bounds::Bounds
    {
        BoundsOperations::get_mut(MenuOperations::get_mut(self))
    }
    fn any
    (
        &self
    ) -> &dyn std::any::Any 
    {
        self
    }
    fn any_mut
    (
        &mut self
    ) -> &mut dyn std::any::Any 
    {
        self
    }
}
impl tui_utilities::utilities_operations::activity_operations::ActivityOperations for crate::menu_materials::materials::exit_menu::ExitMenu
{
    fn get
    (
        &self
    ) -> &tui_utilities::utilities::activity::Activity 
    {
        ActivityOperations::get(MenuOperations::get(self))
    }
    fn get_mut
    (
        &mut self
    ) -> &mut tui_utilities::utilities::activity::Activity 
    {
        ActivityOperations::get_mut(MenuOperations::get_mut(self))
    }
    fn any
    (
        &self
    ) -> &dyn std::any::Any 
    {
        self
    }
    fn any_mut
    (
        &mut self
    ) -> &mut dyn std::any::Any 
    {
        self
    }
}
*/