use thread_manager::materials_operations::MaterialsOperations;
#[allow(unused_imports)]
use tui_utilities::utilities_operations::{menu_operations::MenuOperations, position_operations::PositionOperations, bounds_operations::BoundsOperations, activity_operations::ActivityOperations};

pub trait DeletionMenuOperations:Send + Sync + 'static
{
    fn get_deletion_menu
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<crate::menu_materials::materials::deletion_menu::DeletionMenu>;
    fn get_deletion_menu_embedded
    (
        self:std::sync::Arc<Self>,
        checker:std::sync::Arc<dyn Fn(std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>) -> Option<std::sync::Arc<crate::menu_materials::materials::deletion_menu::DeletionMenu>>>,
    ) -> Option<std::sync::Arc<crate::menu_materials::materials::deletion_menu::DeletionMenu>> where Self:Sized
    {
        checker(self.clone())
    }
    fn any_deletion_menu
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>;
    fn set_cancel_notice
    (
        self:std::sync::Arc<Self>,
        notice:bool
    )  
    {
        *self.clone().get_deletion_menu().cancel_notice.lock().unwrap() = notice;
    }
    fn cancel_noticed
    (
        self:std::sync::Arc<Self>,
    ) -> bool
    {
        self.clone().get_deletion_menu().cancel_notice.lock().unwrap().clone()
    }
    fn set_delete_notice
    (
        self:std::sync::Arc<Self>,
        notice:bool
    )
    {
        *self.clone().get_deletion_menu().delete_notice.lock().unwrap() = notice;
    }
    fn delete_noticed
    (
        self:std::sync::Arc<Self>,
    ) -> bool
    {
        self.clone().get_deletion_menu().delete_notice.lock().unwrap().clone()
    }
}
impl DeletionMenuOperations for crate::menu_materials::materials::deletion_menu::DeletionMenu
{
    fn get_deletion_menu
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::menu_materials::materials::deletion_menu::DeletionMenu> 
    {
        self.clone()    
    }
    fn any_deletion_menu
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::menu_operations::MenuOperations for crate::menu_materials::materials::deletion_menu::DeletionMenu
{
    fn get_menu
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::menu::Menu> 
    {
        self.clone().menu.clone()
    }
    fn any_menu
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::position_operations::PositionOperations for crate::menu_materials::materials::deletion_menu::DeletionMenu
{
    fn get_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::position::Position> 
    {
        self.clone().menu.clone().get_position()    
    }
    fn any_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::bounds_operations::BoundsOperations for crate::menu_materials::materials::deletion_menu::DeletionMenu
{
    fn get_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<tui_utilities::utilities::bounds::Bounds> 
    {
        self.clone().menu.clone().get_bounds()    
    }
    fn any_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::activity_operations::ActivityOperations for crate::menu_materials::materials::deletion_menu::DeletionMenu
{
    fn get_activity
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<tui_utilities::utilities::activity::Activity> 
    {
        self.clone().menu.clone().get_activity()    
    }
    fn any_activity
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl thread_manager::materials_operations::MaterialsOperations for crate::menu_materials::materials::deletion_menu::DeletionMenu
{
    fn get_materials
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<thread_manager::materials::Materials> 
    {
        self.clone().material.clone()    
    }
    fn any_materials
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl thread_manager::hash_key_operations::HashKeyOperations for crate::menu_materials::materials::deletion_menu::DeletionMenu
{
    fn get_hash_key
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<thread_manager::hash_key::HashKey> 
    {
        self.clone().get_materials().get_hash_key()   
    }
    fn any_hash_key
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl thread_manager::handle_operations::HandleOperations for crate::menu_materials::materials::deletion_menu::DeletionMenu
{
    fn get_handle
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<thread_manager::handle::Handle> 
    {
        self.clone().handle.lock().unwrap().as_ref().unwrap().clone()
    }
    fn any_handle 
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
    fn set_handle
    (
        self:std::sync::Arc<Self>,
        handle:&std::sync::Arc<thread_manager::handle::Handle>,
    ) 
    {
        if self.clone().handle_is_some()
        {
            *self.clone().handle.lock().unwrap() = Some(handle.clone());
            return;
        }
        self.clone().get_handle().set_handle(handle);
    }
    fn handle_is_some
    (
        self:std::sync::Arc<Self>,
    ) -> bool 
    {
        self.clone().handle.lock().unwrap().is_some()    
    }
    fn clear_handle
    (
        self:std::sync::Arc<Self>
    ) 
    {
        *self.clone().handle.lock().unwrap() = None
    }
}
impl thread_manager::operator_operations::OperatorOperations for crate::menu_materials::materials::deletion_menu::DeletionMenu
{
    fn get_operator
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<thread_manager::operator::Operator> 
    {
        self.clone().operator.lock().unwrap().as_ref().unwrap().clone()    
    }
    fn any_operator
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl thread_manager::function_list_operations::FunctionListOperations for crate::menu_materials::materials::deletion_menu::DeletionMenu
{
    fn get_function_list 
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<thread_manager::function_list::FunctionList> 
    {
        self.clone().function_list.lock().unwrap().as_ref().unwrap().clone()    
    }
    fn any_function_list
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}