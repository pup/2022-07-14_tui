use tui_utilities::utilities_operations::component_operations::ComponentOperations;

use crate::{component_materials::materials_operations::button_operations::ButtonOperations, menu_materials::materials_operations::deletion_menu_operations::DeletionMenuOperations};

pub trait DeletionMenuServices
{
	fn on_cancel_noticed
	(
		materials:&Vec<std::sync::Arc<dyn thread_manager::materials_operations::MaterialsOperations>>
	);
	fn on_delete_noticed
	(
		materials:&Vec<std::sync::Arc<dyn thread_manager::materials_operations::MaterialsOperations>>
	);
}
impl crate::component_materials::materials_services::button_services::ButtonServices for crate::menu_materials::materials::deletion_menu::DeletionMenu
{
	fn on_click_button
	(
		materials:&Vec<std::sync::Arc<dyn thread_manager::materials_operations::MaterialsOperations>>
	) 
	{
		let deletion_menu = materials[0].clone().any_materials().downcast::<crate::menu_materials::materials::deletion_menu::DeletionMenu>().unwrap();
		let deletion_menu_button =  materials[1].clone().any_materials().downcast::<crate::component_materials::materials::button::Button>().unwrap();
		let standard_input = materials[2].clone().any_materials().downcast::<tui_utilities::utilities_materials::standard_input::StandardInput>().unwrap();
		if deletion_menu_button.clone().get_component_index().unwrap() == 1
		{
			deletion_menu.clone().set_delete_notice(deletion_menu_button.clone().on_click_button(&standard_input))
		}
		if deletion_menu_button.clone().get_component_index().unwrap() == 2
		{
			deletion_menu.clone().set_cancel_notice(deletion_menu_button.clone().on_click_button(&standard_input))
		}
	}
	fn on_enter_button
	(
		materials:&Vec<std::sync::Arc<dyn thread_manager::materials_operations::MaterialsOperations>>
	) 
	{
		let deletion_menu = materials[0].clone().any_materials().downcast::<crate::menu_materials::materials::deletion_menu::DeletionMenu>().unwrap();
		let deletion_menu_button =  materials[1].clone().any_materials().downcast::<crate::component_materials::materials::button::Button>().unwrap();
		let standard_input = materials[2].clone().any_materials().downcast::<tui_utilities::utilities_materials::standard_input::StandardInput>().unwrap();
		if deletion_menu_button.clone().get_component_index().unwrap() == 1
		{
			deletion_menu.clone().set_delete_notice(deletion_menu_button.clone().on_enter_button(&standard_input))
		}
		if deletion_menu_button.clone().get_component_index().unwrap() == 2
		{
			deletion_menu.clone().set_cancel_notice(deletion_menu_button.clone().on_enter_button(&standard_input))
		}
	}
}
impl crate::component_materials::materials_services::label_services::LabelServices for crate::menu_materials::materials::deletion_menu::DeletionMenu
{
	fn draw_label_self
	(
		materials:&Vec<std::sync::Arc<dyn thread_manager::materials_operations::MaterialsOperations>>
	) 
	{
		let deletion_menu_label_trait_object:std::sync::Arc<dyn crate::component_materials::materials_operations::label_operations::LabelOperations> = 
		{
			if materials.clone()[0].clone().get_type_name() == crate::component_materials::materials::button::Button::get_type_name_button()
			{
				materials.clone()[0].clone().any_materials().downcast::<crate::component_materials::materials::button::Button>().unwrap()
			}
			else
			{
				materials.clone()[0].clone().any_materials().downcast::<crate::component_materials::materials::label::Label>().unwrap()
			}
		};
		let standard_input = materials[1].clone().any_materials().downcast::<tui_utilities::utilities_materials::standard_output::StandardOutput>().unwrap();
        deletion_menu_label_trait_object.clone().draw_label_self(&standard_input);
	}
}