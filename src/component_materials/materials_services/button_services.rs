pub trait ButtonServices
{
	fn on_enter_button
	(
	    materials:&Vec<std::sync::Arc<dyn thread_manager::materials_operations::MaterialsOperations>>
	);
	fn on_click_button
	(
	    materials:&Vec<std::sync::Arc<dyn thread_manager::materials_operations::MaterialsOperations>>
	);
}