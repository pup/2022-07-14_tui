pub trait LabelServices
{
	fn draw_label_self
	(
		materials:&Vec<std::sync::Arc<dyn thread_manager::materials_operations::MaterialsOperations>>
	);
}