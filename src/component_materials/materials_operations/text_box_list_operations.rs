use tui_utilities::{utilities_operations::{bounds_operations::BoundsOperations, component_operations::ComponentOperations, position_operations::PositionOperations, listing_bounds_operations::ListingBoundsOperations, indices_operations::IndicesOperations, key_operations::KeyOperations, menu_operations::MenuOperations}, utilities_materials_operations::{standard_input_operations::StandardInputOperations, cursor_operations::CursorOperations}};

use super::text_box_operations::TextBoxOperations;
pub trait TextBoxListOperations:Send + Sync + 'static
{
    fn get_text_box_list
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::component_materials::materials::text_box_list::TextBoxList>;
    fn any_text_box_list
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>;
    fn set_text_box_list
    (
        self:std::sync::Arc<Self>,
        text_box_list:&std::sync::Arc<crate::component_materials::materials::text_box_list::TextBoxList>
    )
    where Self:tui_utilities::utilities_operations::component_operations::ComponentOperations
    {
        *self.clone().get_text_box_list().text_boxes.lock().unwrap() = 
        text_box_list.clone().text_boxes.lock().unwrap().iter()
        .map
        (
            |text_box|
            {
                std::sync::Arc::new 
                (
                    (**text_box).clone()
                )
            }
        ).collect::<Vec<std::sync::Arc<crate::component_materials::materials::text_box::TextBox>>>();
        self.clone().get_text_box_list().set_component(&text_box_list.clone().get_component());
        self.clone().get_text_box_list().set_indices(&text_box_list.clone().get_indices());
        self.clone().get_text_box_list().set_listing_bounds(text_box_list.clone().get_listing_bounds());
        *self.clone().get_text_box_list().largest_text_box.lock().unwrap() = text_box_list.clone().largest_text_box.lock().unwrap().clone();
    }
    fn get_text_box_list_len
    (
        self:std::sync::Arc<Self>
    ) -> usize 
    {
        self.clone().get_text_box_list().text_boxes.lock().unwrap().len()
    }
    fn push_text_box
    (
        self:std::sync::Arc<Self>,
        text_box:std::sync::Arc<crate::component_materials::materials::text_box::TextBox>
    )
    {
        text_box.clone().set_x_position(self.clone().get_text_box_list().get_x_position());
        text_box.clone().set_menu_material_id(self.clone().get_text_box_list().get_menu_material_id());
        text_box.clone().set_component_index(self.clone().get_text_box_list().get_component_index());
        text_box.clone().set_listing_bounds(std::sync::Arc::new((*self.clone().get_text_box_list().get_listing_bounds()).clone()));
        text_box.clone().set_indices(&std::sync::Arc::new((*self.clone().get_text_box_list().get_indices()).clone()));
        self.clone().get_text_box_list().text_boxes.lock().unwrap().push(text_box);
    }
    fn insert_text_box
    (
        self:std::sync::Arc<Self>, 
        index:usize,
        text_box:std::sync::Arc<crate::component_materials::materials::text_box::TextBox>
    )
    {
        if index < self.clone().get_text_box_list_len()
        {
            text_box.clone().set_x_position(self.clone().get_text_box_list().get_x_position());
            text_box.clone().set_menu_material_id(self.clone().get_text_box_list().get_menu_material_id());
            text_box.clone().set_component_index(self.clone().get_text_box_list().get_component_index());
            self.clone().get_text_box_list().text_boxes.lock().unwrap().insert(index, text_box);
        }
        else if index == self.clone().get_text_box_list_len()
        {
            text_box.clone().set_x_position(self.clone().get_text_box_list().get_x_position());
            text_box.clone().set_menu_material_id(self.clone().get_text_box_list().get_menu_material_id());
            text_box.clone().set_component_index(self.clone().get_text_box_list().get_component_index());
            self.clone().get_text_box_list().text_boxes.lock().unwrap().insert(index, text_box);
        }
    }
    fn remove_text_box
    (
        self:std::sync::Arc<Self>, 
        index:usize
    )
    {
        if index < self.clone().get_text_box_list_len()
        {
            self.clone().get_text_box_list().text_boxes.lock().unwrap().remove(index);
        }
        else if index == self.clone().get_text_box_list_len()
        {
            self.clone().get_text_box_list().text_boxes.lock().unwrap().remove(index);
        }
    }
    fn text_box_list_get_current_text_box
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<crate::component_materials::materials::text_box::TextBox>
    {
        self.clone().get_text_box_list().text_boxes.lock().unwrap()[self.clone().get_text_box_list().get_vertical_indices_index().unwrap()].clone()
    }
    fn text_box_list_get_text_box 
    (
        self:std::sync::Arc<Self>,
        index:usize,
    ) -> std::sync::Arc<crate::component_materials::materials::text_box::TextBox>
    {
        self.clone().get_text_box_list().text_boxes.lock().unwrap()[index].clone()
    }
    fn text_box_list_sync_current_text_box
    (
        self:std::sync::Arc<Self>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
    )
    {
        let highlighted_text_box = self.clone().text_box_list_get_current_text_box();
        let adjusted_component_info = std::sync::Arc::new((*self.clone().get_text_box_list().get_component()).clone());
        adjusted_component_info.clone().set_y_position
        (
            Some
            (
                self.clone().get_text_box_list().get_y_position().unwrap() + 
                self.clone().get_text_box_list().calculate_pointer_bounds_distance
                (
                    &self.clone().get_text_box_list().get_position(),
                    &cursor.clone().get_position()
                ).unwrap().1.unwrap()
            )
        );
        adjusted_component_info.clone().set_vertical_bounds_size(1);
        highlighted_text_box.clone().clone().set_component(&adjusted_component_info);
        self.clone().get_text_box_list().set_listing_bounds_left(highlighted_text_box.clone().clone().get_listing_bounds_left());
        self.clone().get_text_box_list().set_listing_bounds_right(highlighted_text_box.clone().clone().get_listing_bounds_right()); 
        self.clone().get_text_box_list().set_indices(&highlighted_text_box.clone().clone().get_indices());
    }
    fn generate_text_box_list_position_array
    (
        self:std::sync::Arc<Self>,
    ) -> Vec<std::sync::Arc<tui_utilities::utilities::position::Position>>
    {
        let mut temp:Vec<std::sync::Arc<tui_utilities::utilities::position::Position>> = Vec::new();
        for index in self.clone().get_text_box_list().generate_bounds_vertical_iter()
        {
            temp.push
            (
                tui_utilities::utilities::position::Position::new_position
                (
                    self.clone().get_text_box_list().get_x_position(), 
                    Some(self.clone().get_text_box_list().get_y_position().unwrap() + index)
                )
            );
        }
        temp
    }
    fn text_box_list_call_get_text_box_input
    (
        self:std::sync::Arc<Self>,
        value:char
    )
    {
        let highlighted_text_box = self.clone().text_box_list_get_current_text_box();
        highlighted_text_box.clone().clone().insert_text_box_character(value);
    }
    fn get_text_box_list_input
    (
        self:std::sync::Arc<Self>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
    )
    {
        for input in standard_input.clone().get_input_stream()
        {
            if input.is_character()
            {
                self.clone().text_box_list_sync_current_text_box(cursor);
                self.clone().text_box_list_call_get_text_box_input(input.get_character().unwrap());
                self.clone().text_box_list_sync_current_text_box(cursor);
                break;
            }
        }
    }
    fn text_box_list_call_eject_text_box_character
    (
        self:std::sync::Arc<Self>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
    )
    {
        let highlighted_text_box = self.clone().text_box_list_get_current_text_box();
        highlighted_text_box.clone().clone().text_box_eject_character(standard_input, cursor);
    }
    fn eject_text_box_list_character
    (
        self:std::sync::Arc<Self>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
    )
    {
        for input in standard_input.clone().get_input_stream()
        {
            if input.is_backspace()
            {
                self.clone().text_box_list_sync_current_text_box(cursor);
                self.clone().text_box_list_call_eject_text_box_character(standard_input, cursor);
                self.clone().text_box_list_sync_current_text_box(cursor);
                break;
            }
        }
    }
    fn draw_text_box_list
    (
        self:std::sync::Arc<Self>, 
        standard_output:&std::sync::Arc<tui_utilities::utilities_materials::standard_output::StandardOutput>
    )
    {
        let listing_bounds = self.clone().get_text_box_list().get_listing_bounds();
        let position_array = self.clone().generate_text_box_list_position_array();
        for text_box_index in 0..position_array.len()
        {
            let current_text_box = self.clone().text_box_list_get_text_box(text_box_index);
            let old_listing_bounds = current_text_box.clone().clone().get_listing_bounds();
            current_text_box.clone().clone().set_listing_bounds(listing_bounds.clone());
            current_text_box.clone().clone().set_position(&position_array[text_box_index].clone());
            current_text_box.clone().clone().draw_text_box(standard_output);
            current_text_box.clone().clone().set_listing_bounds(old_listing_bounds);
        }
    }
    fn text_box_list_call_move_text_box_left
    (
        self:std::sync::Arc<Self>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
    )
    {
        let highlighted_text_box = self.clone().text_box_list_get_current_text_box();
        highlighted_text_box.clone().clone().move_text_box_left(cursor, standard_input)
    }
    fn move_text_box_list_left
    (
        self:std::sync::Arc<Self>, 
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
    )
    {
        for input in standard_input.clone().clone().get_input_stream()
        {
            if input.is_left()
            {
                self.clone().text_box_list_sync_current_text_box(cursor);
                self.clone().text_box_list_call_move_text_box_left(cursor, standard_input);
                self.clone().text_box_list_sync_current_text_box(cursor);
            }
        }
    }
    fn text_box_list_call_move_text_box_right
    (
        self:std::sync::Arc<Self>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
    )
    {
        let highlighted_text_box = self.clone().text_box_list_get_current_text_box();
        highlighted_text_box.clone().clone().move_text_box_right(cursor, standard_input)
    }
    fn move_text_box_list_right
    (
        self:std::sync::Arc<Self>, 
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
    )
    {
        for input in standard_input.clone().clone().get_input_stream()
        {
            if input.is_right()
            {    
                self.clone().text_box_list_sync_current_text_box(cursor);
                self.clone().text_box_list_call_move_text_box_right(cursor, standard_input);
                self.clone().text_box_list_sync_current_text_box(cursor);
            }
        }
    }
    fn move_text_box_list_up
    (
        self:std::sync::Arc<Self>, 
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
    )
    {
        for input in standard_input.clone().clone().get_input_stream()
        {
            if input.is_up()
            {    
                self.clone().text_box_list_sync_current_text_box(cursor);
                cursor.clone().set_component(&std::sync::Arc::new((*self.clone().get_text_box_list().get_component()).clone()));
                cursor.clone().clear_menu();
                let new_position = cursor.clone().get_position();
                new_position.clone().move_position_up(1);
                cursor.clone().move_cursor(&new_position, None);
                self.clone().get_text_box_list().move_listing_bounds_up(1);
                cursor.clone().update_cursor_index(self.clone().get_text_box_list());
                self.clone().text_box_list_sync_current_text_box(cursor);
                break;
            }
        }
    }
    fn move_text_box_list_down
    (
        self:std::sync::Arc<Self>, 
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
    )
    {
        for input in standard_input.clone().clone().get_input_stream()
        {
            if input.is_down()
            {    
                self.clone().text_box_list_sync_current_text_box(cursor);
                cursor.clone().set_component(&std::sync::Arc::new((*self.clone().get_text_box_list().get_component()).clone()));
                cursor.clone().clear_menu();
                let new_position = cursor.clone().get_position();
                new_position.clone().move_position_down(1);
                cursor.clone().move_cursor(&new_position, None);
                self.clone().get_text_box_list().move_listing_bounds_down(1, self.clone().get_text_box_list_len());
                cursor.clone().update_cursor_index(self.clone().get_text_box_list());
                self.clone().text_box_list_sync_current_text_box(cursor);
                break;
            }
        }
    }
    fn get_data
    (
        self:std::sync::Arc<Self>
    ) -> Option<Vec<String>>
    {
        let mut temp:Vec<String> = Vec::new();
        for index in 0..self.clone().get_text_box_list_len()
        {
            temp.push(String::from_iter(self.clone().text_box_list_get_text_box(index).characters.lock().unwrap().iter()));
        }
        Some(temp)
    }
}
impl TextBoxListOperations for crate::component_materials::materials::text_box_list::TextBoxList
{
    fn get_text_box_list
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::component_materials::materials::text_box_list::TextBoxList> 
    {
        self.clone()    
    }
    fn any_text_box_list
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::component_operations::ComponentOperations for crate::component_materials::materials::text_box_list::TextBoxList
{
    fn get_component
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::component::Component> 
    {
        self.clone().component.lock().unwrap().as_ref().unwrap().clone()    
    }
    fn any_component
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()
    }
}
impl tui_utilities::utilities_operations::component_save_operations::ComponentSaveOperations for crate::component_materials::materials::text_box_list::TextBoxList
{
    fn save
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::component_save::ComponentSave> 
    {
        tui_utilities::utilities::component_save::ComponentSave::new_component_save
        (
            self.clone().get_menu_material_id().unwrap(), 
            self.clone().get_material_id().unwrap(), 
            self.clone().get_type_name().unwrap(), 
            self.clone().get_data()
        )    
    }
    fn update
    (
        self:std::sync::Arc<Self>,
        component_save:&std::sync::Arc<tui_utilities::utilities::component_save::ComponentSave>,
        cursor:Option<&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>>
    ) 
    {
        for index in 0..component_save.clone().get_data_len()
        {
            self.clone().push_text_box
            (
                {
                    let component = 
                    tui_utilities::utilities::component::Component::new_component
                    (
                        None, 
                        None, 
                        None, 
                        None, 
                        Some
                        (
                            tui_utilities::utilities::position::Position::new_position(Some(0), Some(0))
                        ), 
                        Some
                        (
                            tui_utilities::utilities::bounds::Bounds::new_bounds(0, self.clone().get_vertical_bounds_size())
                        )
                    );
                    crate::component_materials::materials::text_box::TextBox::new_text_box
                    (
                        component.clone(), 
                        Some(component_save.clone().get_data(index).unwrap().chars().collect::<Vec<char>>()), 
                        Some
                        (
                            tui_utilities::utilities::indices::Indices::new_indices
                            (
                                Some(0), 
                                Some(0)
                            )
                        ), 
                        Some
                        (
                            tui_utilities::utilities::listing_bounds::ListingBounds::new_listing_bounds
                            (
                                Some(0),
                                Some(0),
                                None,
                                None
                            )
                        ), 
                        Some
                        (
                            crate::component_materials::materials_utilities::text_buffer::TextBuffer::new_text_buffer
                            (
                                None, 
                                None, 
                                component.clone()
                            )
                        ), 
                        cursor.clone().unwrap()
                    )
                }
            )
        }
    }
}
impl tui_utilities::utilities_operations::position_operations::PositionOperations for crate::component_materials::materials::text_box_list::TextBoxList
{
    fn get_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::position::Position> 
    {
        self.clone().get_component().get_position()    
    }
    fn any_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::bounds_operations::BoundsOperations for crate::component_materials::materials::text_box_list::TextBoxList
{
    fn get_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<tui_utilities::utilities::bounds::Bounds> 
    {
        self.clone().get_component().get_bounds()    
    }
    fn any_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::indices_operations::IndicesOperations for crate::component_materials::materials::text_box_list::TextBoxList
{
    fn get_indices
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<tui_utilities::utilities::indices::Indices> 
    {
        self.clone().indices.lock().unwrap().as_ref().unwrap().clone()    
    }
    fn any_indices
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::listing_bounds_operations::ListingBoundsOperations for crate::component_materials::materials::text_box_list::TextBoxList
{
    fn get_listing_bounds
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::listing_bounds::ListingBounds> 
    {
        self.clone().listing_bounds.lock().unwrap().as_ref().unwrap().clone()    
    }
    fn any_list_bounds
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}