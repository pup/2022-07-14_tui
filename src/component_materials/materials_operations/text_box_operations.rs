use tui_utilities::{utilities_operations::{component_operations::ComponentOperations, indices_operations::IndicesOperations, listing_bounds_operations::ListingBoundsOperations, bounds_operations::BoundsOperations, position_operations::PositionOperations, menu_operations::MenuOperations, key_operations::KeyOperations, timer_operations::TimerOperations}, utilities_materials_operations::{standard_output_operations::StandardOutputOperations, standard_input_operations::StandardInputOperations, cursor_operations::CursorOperations}};

use crate::component_materials::{materials::text_box::TextBox, materials_utilities_operations::text_buffer_operations::TextBufferOperations};

pub trait TextBoxOperations:Send + Sync + 'static
{
    fn get_text_box
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::component_materials::materials::text_box::TextBox>;
    fn any_text_box
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>;
    fn set_text_box
    (
        self:std::sync::Arc<Self>,
        text_box:&std::sync::Arc<crate::component_materials::materials::text_box::TextBox>
    )
    {
        *self.clone().get_text_box().characters.lock().unwrap() = text_box.characters.lock().unwrap().clone();
        self.clone().get_text_box().set_component(&text_box.clone().get_component());
        self.clone().get_text_box().set_indices(&text_box.clone().get_indices());
        self.clone().get_text_box().set_listing_bounds(text_box.clone().get_listing_bounds());
        self.clone().get_text_box().set_text_buffer(&text_box.clone().get_text_buffer());
    }
    fn push_text_box_character
    (
        self:std::sync::Arc<Self>,
        character:char
    )
    {
        self.clone().get_text_box().characters.lock().unwrap().push(character)
    }
    fn insert_text_box_character
    (
        self:std::sync::Arc<Self>, 
        value:char
    )
    {
        let temp = self.clone().get_text_box().get_horizontal_indices_index().unwrap();
        self.clone().get_text_box().characters.lock().unwrap()[temp] = value;
    }
    fn remove_text_box_character
    (
        self:std::sync::Arc<Self>
    )
    {
        let temp = self.clone().get_text_box().get_horizontal_indices_index().unwrap();
        self.clone().get_text_box().characters.lock().unwrap().remove(temp);
    }
    fn get_text_box_character
    (
        self:std::sync::Arc<Self>,
        index:usize
    ) -> char
    {
        self.clone().get_text_box().characters.lock().unwrap()[index].clone()
    }
    fn get_text_box_characters_len
    (
        self:std::sync::Arc<Self>
    ) -> usize
    {
        self.clone().get_text_box().characters.lock().unwrap().len()
    }
    fn set_text_box_left_to_zero
    (
        self:std::sync::Arc<Self>
    )
    {
        self.clone().get_text_box().set_listing_bounds_left(Some(0))
    }
    fn set_text_box_left_to_list_entry_length
    (
        self:std::sync::Arc<Self>
    )
    {
        self.clone().get_text_box().set_listing_bounds_left(Some(self.clone().get_text_box().characters.lock().unwrap().len() - 1))
    }
    fn draw_text_box
    (
        self:std::sync::Arc<Self>, 
        standard_output:&std::sync::Arc<tui_utilities::utilities_materials::standard_output::StandardOutput>,
    )
    {
        let mut temp_string = String::new();
        for index in 0..self.clone().get_text_box_characters_len()
        {
            
            if self.clone().get_text_box().index_in_horizontal_listing_bounds(index)
            {
                temp_string.push(self.clone().get_text_box_character(index));
            }
        }
        standard_output.clone().print_words(temp_string, self.clone().get_text_box().get_position());
    }
    fn move_text_box_left
    (
        self:std::sync::Arc<Self>, 
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
    )
    {
        if self.clone().get_text_box().listing_bounds_is_some() && self.clone().get_text_box().indices_is_some()
        {
            for input in standard_input.clone().get_input_stream()
            {
                if input.is_left()
                {
                    cursor.clone().set_component(&self.clone().get_text_box().get_component());
                    cursor.clone().clear_menu();
                    let new_position = cursor.clone().get_position();
                    new_position.clone().move_position_left(1);
                    cursor.clone().move_cursor
                    (
                        &new_position,
                        Some
                        (
                            self.clone().get_text_box_characters_len()
                        )
                    );
                    if  cursor.clone().get_component().pointer_on_bounds_left
                    (
                        &self.clone().get_text_box().get_position(),
                        &cursor.clone().get_position()
                    ) 
                    {
                        self.clone().get_text_box().move_listing_bounds_left(1);
                    }
                    cursor.clone().update_cursor_index
                    (
                        self.clone().get_text_box()
                    );
                    break;
                }
            }
        }
    }
    fn move_text_box_right
    (
        self:std::sync::Arc<Self>, 
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
    )
    {
        if self.clone().get_text_box().listing_bounds_is_some() && self.clone().get_text_box().indices_is_some()
        {
            for input in standard_input.clone().get_input_stream()
            {
                if input.is_right()
                {
                    cursor.clone().set_component(&self.clone().get_text_box().get_component());
                    cursor.clone().clear_menu();
                    let new_position = cursor.clone().get_position();
                    new_position.clone().move_position_right(1);
                    cursor.clone().move_cursor
                    (
                        &new_position, 
                        Some
                        (
                            self.clone().get_text_box().characters.lock().unwrap().len()
                        )
                    );
                    if  cursor.clone().get_component().pointer_on_bounds_right
                    (
                        &self.clone().get_text_box().get_position(),
                        &cursor.clone().get_position()
                    )
                    {
                        self.clone().get_text_box().move_listing_bounds_right
                        (
                            1,
                            self.clone().get_text_box().get_text_box_characters_len()
                        );
                    }
                    cursor.clone().update_cursor_index
                    (
                        self.clone().get_text_box()
                    );
                    break;
                }
            }
        }
    }
    fn text_box_get_input
    (
        self:std::sync::Arc<Self>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>
    )
    {
        for input in standard_input.clone().get_input_stream()
        {
            if input.is_character()
            {
                cursor.clone().set_component(&self.clone().get_text_box().get_component());
                cursor.clone().clear_menu();
                self.clone().insert_text_box_character(input.get_character().unwrap());
                let new_position = cursor.clone().get_position();
                new_position.clone().move_position_right(1);
                cursor.clone().move_cursor
                (
                    &new_position,
                    Some
                    (
                        self.clone().get_text_box().get_text_box_characters_len()
                    )
                );
                if  cursor.clone().get_component().pointer_on_bounds_right
                (
                    &self.clone().get_text_box().get_position(),
                    &cursor.clone().get_position()
                )
                {
                    self.clone().get_text_box().move_listing_bounds_right
                    (
                        1,
                        self.clone().get_text_box().get_text_box_characters_len()
                    );
                }
                cursor.clone().update_cursor_index
                (
                    self.clone().get_text_box()
                );
                //Add timer disable feature.
                self.clone().get_text_box().get_text_buffer().start_timer();
                break;
            }
        }
    }
    fn text_box_eject_character
    (
        self:std::sync::Arc<Self>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>
    )
    {
        for input in standard_input.clone().get_input_stream()
        {
            if input.is_backspace()
            {
                cursor.clone().set_component(&self.clone().get_text_box().get_component());
                cursor.clone().clear_menu();
                self.clone().remove_text_box_character();
                let new_position = cursor.clone().get_position();
                new_position.clone().move_position_left(1);
                cursor.clone().move_cursor
                (
                    &new_position, 
                    Some
                    (
                        self.clone().get_text_box().characters.lock().unwrap().len()
                    )
                );
                self.clone().get_text_box().move_listing_bounds_left(1);
                cursor.clone().update_cursor_index
                (
                    self.clone().get_text_box()
                );
                //Add timer disable feature.
                self.clone().get_text_box().get_text_buffer().start_timer();
                break;
            }
        }
    }
    fn get_data
    (
        self:std::sync::Arc<Self>
    ) -> Option<Vec<String>>
    {
        let length = self.clone().get_text_box().get_text_buffer().get_text_buffer_len();
        let temp:Vec<String> = vec![String::from_iter(self.clone().get_text_box().get_text_buffer().buffer.lock().unwrap()[length - 1].iter())];
        Some(temp)
    }
}
impl TextBoxOperations for crate::component_materials::materials::text_box::TextBox
{
    fn get_text_box
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::component_materials::materials::text_box::TextBox> 
    {
        self.clone()    
    }
    fn any_text_box
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::component_operations::ComponentOperations for crate::component_materials::materials::text_box::TextBox
{
    fn get_component
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::component::Component> 
    {
        self.clone().component.lock().unwrap().as_ref().unwrap().clone()
    }
    fn any_component
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
    fn clear_component
    (
        self:std::sync::Arc<Self>
    ) 
    {
        *self.clone().component.lock().unwrap() = None;
    }
    fn component_is_some
    (
        self:std::sync::Arc<Self>
    ) -> bool 
    {
        self.clone().component.lock().unwrap().is_some()    
    }
}
impl tui_utilities::utilities_operations::component_save_operations::ComponentSaveOperations for crate::component_materials::materials::text_box::TextBox
{
    fn save
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::component_save::ComponentSave> 
    {
        tui_utilities::utilities::component_save::ComponentSave::new_component_save
        (
            self.clone().get_menu_material_id().unwrap(), 
            self.clone().get_material_id().unwrap(), 
            self.clone().get_type_name().unwrap(), 
            self.clone().get_data()
        )
    }
    fn update
    (
        self:std::sync::Arc<Self>,
        component_save:&std::sync::Arc<tui_utilities::utilities::component_save::ComponentSave>,
        _cursor:Option<&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>>
    ) 
    {
        let temp = component_save.get_data(0).unwrap();
        for index in 0..temp.len() - 1
        {
            self.clone().push_text_box_character(temp.get(index..index).unwrap().chars().next().unwrap());
        }
    }
}
impl tui_utilities::utilities_operations::position_operations::PositionOperations for crate::component_materials::materials::text_box::TextBox
{
    fn get_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::position::Position> 
    {
        self.clone().get_component().get_position()
    }
    fn any_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::bounds_operations::BoundsOperations for crate::component_materials::materials::text_box::TextBox
{
    fn get_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<tui_utilities::utilities::bounds::Bounds> 
    {
        self.clone().get_component().get_bounds()
    }
    fn any_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::listing_bounds_operations::ListingBoundsOperations for crate::component_materials::materials::text_box::TextBox
{
    fn get_listing_bounds
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::listing_bounds::ListingBounds> 
    {
        self.clone().listing_bounds.lock().unwrap().as_ref().unwrap().clone()
    }
    fn any_list_bounds
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
    fn clear_listing_bounds
    (
        self:std::sync::Arc<Self>
    ) 
    {
        *self.clone().listing_bounds.lock().unwrap() = None;    
    }
    fn listing_bounds_is_some
    (
        self:std::sync::Arc<Self>
    ) -> bool 
    {
        self.clone().listing_bounds.lock().unwrap().is_some()
    }
}
impl tui_utilities::utilities_operations::indices_operations::IndicesOperations for crate::component_materials::materials::text_box::TextBox
{
    fn get_indices
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<tui_utilities::utilities::indices::Indices> 
    {
        self.clone().indices.lock().unwrap().as_ref().unwrap().clone()
    }
    fn any_indices
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
    fn clear_indices
    (
        self:std::sync::Arc<Self>
    ) 
    {
        *self.clone().indices.lock().unwrap() = None;
    }
    fn indices_is_some
    (
        self:std::sync::Arc<Self>
    ) -> bool 
    {
        self.clone().indices.lock().unwrap().is_some()    
    }
}
impl crate::component_materials::materials_utilities_operations::text_buffer_operations::TextBufferOperations for crate::component_materials::materials::text_box::TextBox
{
    fn get_text_buffer
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::component_materials::materials_utilities::text_buffer::TextBuffer> 
    {
        self.clone().text_buffer.lock().unwrap().as_ref().unwrap().clone()
    }
    fn any_text_buffer
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
    fn clear_text_buffer
    (
        self:std::sync::Arc<Self>,
    ) 
    {
        *self.clone().text_buffer.lock().unwrap() = None;   
    }
    fn text_buffer_is_some
    (
        self:std::sync::Arc<Self>,
    ) -> bool 
    {
        self.clone().text_buffer.lock().unwrap().is_some()    
    }
}
impl Clone for crate::component_materials::materials::text_box::TextBox
{
    fn clone
    (
        &self
    ) -> crate::component_materials::materials::text_box::TextBox 
    {
        TextBox 
        { 
            characters:
            std::sync::Arc::new 
            (
                std::sync::Mutex::new
                (
                    self.characters.lock().unwrap().clone()
                )
            ), 
            component:
            {
                if self.component.lock().unwrap().is_none()
                {
                    std::sync::Mutex::new 
                    (
                        self.component.lock().unwrap().clone()
                    )
                }
                else
                {
                    std::sync::Mutex::new 
                    (
                        Some 
                        (
                            std::sync::Arc::new 
                            (
                                (**self.component.lock().unwrap().as_ref().unwrap()).clone()
                            )
                        )
                    )
                }
            },
            indices:
            {
                if self.indices.lock().unwrap().is_none()
                {
                    std::sync::Mutex::new 
                    (
                        self.indices.lock().unwrap().clone()
                    )
                }
                else 
                {
                    std::sync::Mutex::new 
                    (
                        Some 
                        (
                            std::sync::Arc::new 
                            (
                                (**self.indices.lock().unwrap().as_ref().unwrap()).clone()
                            )
                        )
                    )
                }
            },
            listing_bounds:
            {
                if self.listing_bounds.lock().unwrap().is_none()
                {
                    std::sync::Mutex::new 
                    (
                        self.listing_bounds.lock().unwrap().clone()
                    )
                }
                else 
                {
                    std::sync::Mutex::new 
                    (
                        Some 
                        (
                            std::sync::Arc::new 
                            (
                                (**self.listing_bounds.lock().unwrap().as_ref().unwrap()).clone()
                            )
                        )
                    )
                }
            },
            text_buffer:
            {
                if self.text_buffer.lock().unwrap().is_none()
                {
                    std::sync::Mutex::new 
                    (
                        self.text_buffer.lock().unwrap().clone()
                    )
                }
                else 
                {
                    std::sync::Mutex::new 
                    (
                        Some 
                        (
                            std::sync::Arc::new 
                            (
                                (**self.text_buffer.lock().unwrap().as_ref().unwrap()).clone()
                            )
                        )
                    )
                }
            },
        }
    }
}