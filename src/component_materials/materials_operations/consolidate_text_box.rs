 pub trait ConsolidateTextBox 
{
    fn test_for_date
    (
        text_box:&std::sync::Arc<crate::component_materials::materials::text_box::TextBox>,
    ) -> bool
    {
        let date_pattern = regex::Regex::new(r"[0-9]{4}-[0-9]{2}-[0-9]{2}").unwrap();
        let text = String::from_iter(text_box.characters.lock().unwrap().clone().iter());
        let matched = date_pattern.find(text.as_str());
        matched.is_some()
    }
    fn test_for_numbering
    (
        text_box:&std::sync::Arc<crate::component_materials::materials::text_box::TextBox>,
    ) -> bool
    {
        let date_pattern = regex::Regex::new(r"[0-9]{4}").unwrap();
        let text = String::from_iter(text_box.characters.lock().unwrap().clone().iter());
        let matched = date_pattern.find(text.as_str());
        matched.is_some()
    }
    fn convert_to_upper_lower_string
    (
        text_box:&std::sync::Arc<crate::component_materials::materials::text_box::TextBox>,
    ) -> String
    {
        let mut adjusted_text = String::new();
        adjusted_text.push('_');
        let date_pattern = regex::Regex::new(r"[^\s]+").unwrap();
        let text = String::from_iter(text_box.characters.lock().unwrap().clone().iter());
        for data in date_pattern.find_iter(&text)
        {
            let mut temp = String::from(data.as_str());
            temp.insert_str(0,temp.get(0..1).as_ref().unwrap().to_uppercase().as_str());
            adjusted_text.push_str(temp.as_str());
        }
        adjusted_text
    }
}