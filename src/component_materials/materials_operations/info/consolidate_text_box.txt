The trait will take a string then pattern match
the string based on my standard of "Date_Name_Number".
The trait will check for accurate conversion for
date syntax (yyyy-mm-dd). The trait will check  for 
accurate numbering syntax (###). The trait will take 
a text box and convert the data to a string matching on 
of the checks above, or just a first letter upper case 
list of all the words in the text box.