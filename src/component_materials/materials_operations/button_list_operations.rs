use tui_utilities::{utilities_operations::{component_operations::ComponentOperations, indices_operations::IndicesOperations, listing_bounds_operations::ListingBoundsOperations, position_operations::PositionOperations, bounds_operations::BoundsOperations, menu_operations::MenuOperations}, utilities_materials_operations::{cursor_operations::CursorOperations, standard_input_operations::StandardInputOperations}};

use super::{label_operations::LabelOperations, button_operations::ButtonOperations};

pub trait ButtonListOperations
{
    fn get_button_list
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::component_materials::materials::button_list::ButtonList>;
    fn any_button_list
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>;
    fn set_button_list
    (
        self:std::sync::Arc<Self>,
        button_list:&std::sync::Arc<crate::component_materials::materials::button_list::ButtonList>
    )
    {
        *self.clone().get_button_list().list.lock().unwrap() = button_list.list.lock().unwrap().clone();
        self.clone().get_button_list().set_component(&button_list.clone().get_component());
        self.clone().get_button_list().set_listing_bounds(button_list.clone().get_listing_bounds());
        self.clone().get_button_list().set_indices(&button_list.clone().get_indices());
    }
    fn get_button_list_len
    (
        self:std::sync::Arc<Self>
    ) -> usize
    {
        self.clone().get_button_list().list.lock().unwrap().len()
    }
    fn push_button
    (
        self:std::sync::Arc<Self>,
        value:std::sync::Arc<crate::component_materials::materials::button::Button>
    )
    {
        value.clone().set_x_position(self.clone().get_button_list().get_x_position());
        self.clone().get_button_list().list.lock().unwrap().push(value)
    }
    fn insert_button
    (
        self:std::sync::Arc<Self>, 
        index:usize,
        entry:std::sync::Arc<crate::component_materials::materials::button::Button>
    )
    {
        if index < self.clone().get_button_list().get_button_list_len()
        {
            entry.clone().set_x_position(self.clone().get_button_list().get_x_position());
            self.clone().get_button_list().list.lock().unwrap().insert(index, entry)
        }
        else if index == self.clone().get_button_list().get_button_list_len()
        {
            entry.clone().set_x_position(self.clone().get_button_list().get_x_position());
            self.clone().get_button_list().push_button(entry)
        }
    }
    fn remove_button
    (
        self:std::sync::Arc<Self>, 
        index:usize
    )
    {
        if index < self.clone().get_button_list().get_button_list_len()
        {
            self.clone().get_button_list().list.lock().unwrap().remove(index);
        }
        else if index == self.clone().get_button_list().get_button_list_len()
        {
            self.clone().get_button_list().list.lock().unwrap().pop();
        }
    }
    fn get_button_from_list
    (
        self:std::sync::Arc<Self>,
        index:usize
    ) -> Option<std::sync::Arc<crate::component_materials::materials::button::Button>>
    {
        if index < self.clone().get_button_list().get_button_list_len()
        {
            Some
            (
                self.clone().get_button_list().list.lock().unwrap()[index].clone()
            )
        }
        else if index >= self.clone().get_button_list().get_button_list_len()
        {
            None
        }
        else 
        {
            None
        }
    }
    fn draw_text_list
    (
        self:std::sync::Arc<Self>, 
        standard_output:&std::sync::Arc<tui_utilities::utilities_materials::standard_output::StandardOutput>
    )
    {
        for index in 0..self.clone().get_button_list().get_button_list_len()
        {
            if index <= self.clone().get_button_list().clone().get_listing_bounds_up().unwrap() && index >= self.clone().get_button_list().clone().get_listing_bounds_down().unwrap() {continue;}
            let x = Some(self.clone().get_button_list().clone().get_y_position().unwrap() + (index - self.clone().get_button_list().clone().get_listing_bounds_up().unwrap()));
            self.clone().get_button_from_list(index).unwrap().set_x_position(x);
            self.clone().get_button_from_list(index).unwrap().draw_label_self(standard_output);
            self.clone().get_button_from_list(index).unwrap().set_x_position(None);
        }
    }
    fn move_button_list_up
    (
        self:std::sync::Arc<Self>, 
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>
    )
    {
        for input in standard_input.clone().get_input_stream()
        {
            if let termion::event::Event::Key(termion::event::Key::Left) = input
            {
                cursor.clone().set_component(&self.clone().get_button_list().clone().get_component());
                cursor.clone().clear_menu();
                let new_position = cursor.clone().get_position();
                new_position.clone().move_position_up(1);
                cursor.clone().move_cursor(&new_position, None);
                if  cursor.clone().get_component().pointer_on_bounds_top
                    (
                        &self.clone().get_button_list().get_position(),
                        &cursor.clone().get_position()
                    )
                {
                    self.clone().get_button_list().move_listing_bounds_up(1);
                }
                cursor.clone().update_cursor_index(self.clone().get_button_list());
            }
        }
    }
    fn move_button_list_down
    (
        self:std::sync::Arc<Self>, 
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>
    )
    {
        for input in standard_input.clone().get_input_stream()
        {
            if let termion::event::Event::Key(termion::event::Key::Left) = input
            {
                cursor.clone().set_component(&self.clone().get_button_list().clone().get_component());
                cursor.clone().clear_menu();
                let new_position = cursor.clone().get_position();
                new_position.clone().move_position_down(1);
                cursor.clone().move_cursor(&new_position, None);
                if  cursor.clone().get_component().pointer_on_bounds_bottom
                    (
                        &self.clone().get_button_list().get_position(),
                        &cursor.clone().get_position()
                    )
                {
                    self.clone().get_button_list().move_listing_bounds_down(1, self.clone().get_button_list_len());
                };
                cursor.clone().update_cursor_index(self.clone().get_button_list());
            }
        }
    }
    fn on_enter_button_list
    (
        self:std::sync::Arc<Self>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>
    ) -> (usize, bool)
    {
        let current_index = self.clone().get_button_list().get_vertical_indices_index().unwrap();
        (current_index, self.clone().get_button_from_list(current_index).unwrap().on_enter_button(standard_input))
    }
    fn on_click_button_list
    (
        self:std::sync::Arc<Self>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>
    ) -> Option<usize>
    {
        for index in 0..self.clone().get_button_list().get_button_list_len()
        {
            if  index <= self.clone().get_button_list().clone().get_listing_bounds_up().unwrap() && 
                index >= self.clone().get_button_list().clone().get_listing_bounds_down().unwrap() 
            {
                continue;
            }
            if  self.clone().get_button_from_list(index).unwrap().on_click_button(standard_input) 
            {
                return Some(index);
            }
        }
        None
    }
    fn get_data
    (
        self:std::sync::Arc<Self>
    ) -> Option<Vec<String>>
    {
        let mut temp:Vec<String> = Vec::new();
        for index in 0..self.clone().get_button_list().get_button_list_len()
        {
            temp.push
            (
                {
                    if self.clone().get_button_from_list(index).unwrap().get_label_string().is_none()
                    {
                        String::new()
                    }
                    else 
                    {
                        self.clone().get_button_from_list(index).unwrap().get_label_string().unwrap()
                    }
                }
            );
        }
        Some(temp)
    }
}
impl ButtonListOperations for crate::component_materials::materials::button_list::ButtonList
{
    fn get_button_list
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::component_materials::materials::button_list::ButtonList>
    {
        self.clone()    
    }
    fn any_button_list
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone().get_button_list().clone()    
    }
}
impl tui_utilities::utilities_operations::component_operations::ComponentOperations for crate::component_materials::materials::button_list::ButtonList
{
    fn get_component
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::component::Component> 
    {
        self.clone().get_button_list().component.clone()    
    }
    fn any_component
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone().get_button_list().clone()    
    }
}
impl thread_manager::materials_operations::MaterialsOperations for crate::component_materials::materials::button_list::ButtonList
{
    fn get_materials
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<thread_manager::materials::Materials> 
    {
        self.clone().get_button_list().material.clone()    
    }
    fn any_materials
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone().get_button_list().clone()
    }
}
impl tui_utilities::utilities_operations::component_save_operations::ComponentSaveOperations for crate::component_materials::materials::button_list::ButtonList
{
    fn save
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::component_save::ComponentSave>
    {
        tui_utilities::utilities::component_save::ComponentSave::new_component_save
        (
            self.clone().get_button_list().clone().get_menu_material_id().unwrap(), 
            self.clone().get_button_list().clone().get_material_id().unwrap(), 
            self.clone().get_button_list().clone().get_type_name().unwrap(), 
            self.clone().get_button_list().clone().get_data()
        )
    }
    fn update
    (
        self:std::sync::Arc<Self>,
        component_save:&std::sync::Arc<tui_utilities::utilities::component_save::ComponentSave>,
        _cursor:Option<&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>>
    ) 
    {
        for index in 0..component_save.get_data_len()
        {
            self.clone().get_button_list().push_button
            (
                crate::component_materials::materials::button::Button::new_button
                (
                    component_save.get_data(index).clone(),
                    Some
                    (
                        self.clone().get_button_list().clone().get_component()
                    )
                )
            );
        }    
    }
}
impl tui_utilities::utilities_operations::position_operations::PositionOperations for crate::component_materials::materials::button_list::ButtonList
{
    fn get_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::position::Position> 
    {
        self.clone().get_button_list().get_component().get_position()    
    }
    fn any_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone().get_button_list().clone()    
    }
}
impl tui_utilities::utilities_operations::bounds_operations::BoundsOperations for crate::component_materials::materials::button_list::ButtonList
{
    fn get_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<tui_utilities::utilities::bounds::Bounds> 
    {
        self.clone().get_button_list().get_component().get_bounds()    
    } 
    fn any_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone().get_button_list().clone()    
    }
}
impl tui_utilities::utilities_operations::listing_bounds_operations::ListingBoundsOperations for crate::component_materials::materials::button_list::ButtonList
{
    fn get_listing_bounds
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::listing_bounds::ListingBounds> 
    {
        self.clone().get_button_list().listing_bounds.clone()    
    }
    fn any_list_bounds
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone().get_button_list().clone()    
    }
}
impl tui_utilities::utilities_operations::indices_operations::IndicesOperations for crate::component_materials::materials::button_list::ButtonList
{
    fn get_indices
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<tui_utilities::utilities::indices::Indices> 
    {
        self.clone().get_button_list().indices.clone()    
    }
    fn any_indices
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone().get_button_list().clone()    
    }
}