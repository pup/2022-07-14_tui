use tui_utilities::{utilities_operations::{component_operations::ComponentOperations, position_operations::PositionOperations}, utilities_materials_operations::standard_output_operations::StandardOutputOperations};
pub trait LabelOperations:Send + Sync + 'static
{
    fn get_label
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::component_materials::materials::label::Label>;
    fn any_label
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>;
    fn set_label
    (
        self:std::sync::Arc<Self>,
        label:&std::sync::Arc<crate::component_materials::materials::label::Label>
    )
    {
        *self.clone().get_label().text.lock().unwrap() = label.text.lock().unwrap().clone();
        *self.clone().get_label().component.lock().unwrap() = label.component.lock().unwrap().clone();
    }
    fn get_label_string
    (
        self:std::sync::Arc<Self>
    ) -> Option<String>
    {
        if self.clone().get_label().text.lock().unwrap().is_some()
        {
            return self.clone().get_label().text.lock().unwrap().clone();
        }
        None
    }
    fn set_label_string
    (
        self:std::sync::Arc<Self>, 
        value:Option<String>
    ) -> Option<String>
    {
        let temp = self.clone().get_label().text.lock().unwrap().clone();
        *self.clone().get_label().text.lock().unwrap() = value;
        temp
    }
    fn draw_label_self
    (
        self:std::sync::Arc<Self>, 
        standard_output:&std::sync::Arc<tui_utilities::utilities_materials::standard_output::StandardOutput>
    )
    {
        if self.clone().get_label().text_is_some() 
        {
            standard_output.clone().print_words
            (
                self.clone().get_label_string().unwrap(), 
                self.clone().get_label().get_position(),
            );
        }
    }
    fn text_is_some
    (
        self:std::sync::Arc<Self>
    ) -> bool
    {
        self.clone().get_label().text.lock().unwrap().is_some()
    }
    fn get_data
    (
        self:std::sync::Arc<Self>
    ) -> Option<Vec<String>>
    {
        Some
        (
            vec!
            [
                self.clone().get_label().get_label_string().unwrap()
            ]
        )
    }
}
impl LabelOperations for crate::component_materials::materials::label::Label
{
    fn get_label
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::component_materials::materials::label::Label> 
    {
        self.clone()    
    }
    fn any_label
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::component_operations::ComponentOperations for crate::component_materials::materials::label::Label
{
    fn get_component
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::component::Component> 
    {
        self.clone().get_label().component.lock().unwrap().as_ref().unwrap().clone()
    }
    fn any_component
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()
    }
    fn clear_component
    (
        self:std::sync::Arc<Self>
    ) 
    {
        *self.clone().get_label().component.lock().unwrap() = None
    }
    fn component_is_some
    (
        self:std::sync::Arc<Self>
    ) -> bool 
    {
        self.clone().get_label().component.lock().unwrap().is_some()    
    }
}
impl tui_utilities::utilities_operations::component_save_operations::ComponentSaveOperations for crate::component_materials::materials::label::Label
{
    fn save
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::component_save::ComponentSave>
    {
        tui_utilities::utilities::component_save::ComponentSave::new_component_save(
            self.clone().get_menu_material_id().unwrap(), 
            self.clone().get_component_index().unwrap(), 
            self.clone().get_type_name().unwrap(), 
            self.clone().get_data()
        )
    }
    fn update
    (
        self:std::sync::Arc<Self>,
        component_save:&std::sync::Arc<tui_utilities::utilities::component_save::ComponentSave>,
        _cursor:Option<&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>>
    ) 
    {
        self.clone().set_label_string(Some(component_save.get_data(0).unwrap()));
    }
}
impl tui_utilities::utilities_operations::position_operations::PositionOperations for crate::component_materials::materials::label::Label
{
    fn get_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::position::Position> 
    {
        self.clone().get_component().get_position()    
    }
    fn any_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::bounds_operations::BoundsOperations for crate::component_materials::materials::label::Label
{
    fn get_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<tui_utilities::utilities::bounds::Bounds> 
    {
        self.clone().get_component().get_bounds()    
    }
    fn any_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl thread_manager::materials_operations::MaterialsOperations for crate::component_materials::materials::label::Label
{
    fn get_materials
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<thread_manager::materials::Materials> 
    {
        self.material.clone()    
    }
    fn any_materials
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}