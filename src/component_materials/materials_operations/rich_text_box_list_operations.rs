use tui_utilities::{utilities_operations::{component_operations::ComponentOperations, indices_operations::IndicesOperations, listing_bounds_operations::ListingBoundsOperations, position_operations::PositionOperations, bounds_operations::BoundsOperations, key_operations::KeyOperations, menu_operations::MenuOperations}, utilities_materials_operations::{standard_input_operations::StandardInputOperations, cursor_operations::CursorOperations}};

use super::text_box_operations::TextBoxOperations;

pub trait RichTextBoxListOperations:Send + Sync + 'static
{
    fn get_rich_text_box_list
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<crate::component_materials::materials::rich_text_box_list::RichTextBoxList>;
    fn any_rich_text_box_list
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>;
    fn set_rich_text_box_list
    (
        self:std::sync::Arc<Self>,
        rich_text_box_list:&std::sync::Arc<crate::component_materials::materials::rich_text_box_list::RichTextBoxList>,
    )
    {
        *self.clone().get_rich_text_box_list().text_boxes.lock().unwrap() = 
        rich_text_box_list.clone().text_boxes.lock().unwrap().iter()
        .map
        (
            |text_box|
            {
                std::sync::Arc::new 
                (
                    (**text_box).clone()
                )
            }
        ).collect::<Vec<std::sync::Arc<crate::component_materials::materials::text_box::TextBox>>>();
        self.clone().get_rich_text_box_list().set_component(&rich_text_box_list.clone().get_component());
        self.clone().get_rich_text_box_list().set_indices(&rich_text_box_list.clone().get_indices());
        self.clone().get_rich_text_box_list().set_listing_bounds(rich_text_box_list.clone().get_listing_bounds());
        *self.clone().get_rich_text_box_list().largest_text_box.lock().unwrap() = rich_text_box_list.clone().largest_text_box.lock().unwrap().clone()
    }
    fn get_rich_text_box_list_len
    (
        self:std::sync::Arc<Self>
    ) -> usize 
    {
        self.clone().get_rich_text_box_list().text_boxes.lock().unwrap().len()
    }
    fn push_rich_text_box_text_box
    (
        self:std::sync::Arc<Self>,
        rich_text_box:std::sync::Arc<crate::component_materials::materials::text_box::TextBox>
    )
    {
        rich_text_box.clone().set_x_position(self.clone().get_rich_text_box_list().get_x_position());
        rich_text_box.clone().set_menu_material_id(self.clone().get_rich_text_box_list().get_menu_material_id());
        rich_text_box.clone().set_component_index(self.clone().get_rich_text_box_list().get_component_index());
        rich_text_box.clone().set_listing_bounds(self.clone().get_rich_text_box_list().get_listing_bounds());
        rich_text_box.clone().set_indices(&self.clone().get_rich_text_box_list().get_indices());
        self.clone().get_rich_text_box_list().text_boxes.lock().unwrap().push(rich_text_box);
    }
    fn insert_rich_text_box_text_box
    (
        self:std::sync::Arc<Self>,
        index:usize,
        rich_text_box:std::sync::Arc<crate::component_materials::materials::text_box::TextBox>
    )
    {
        if index < self.clone().get_rich_text_box_list_len()
        {
            rich_text_box.clone().set_x_position(self.clone().get_rich_text_box_list().get_x_position());
            rich_text_box.clone().set_menu_material_id(self.clone().get_rich_text_box_list().get_menu_material_id());
            rich_text_box.clone().set_component_index(self.clone().get_rich_text_box_list().get_component_index());
            self.clone().get_rich_text_box_list().text_boxes.lock().unwrap().insert(index, rich_text_box);
        }
        else if index >= self.clone().get_rich_text_box_list_len()
        {
            rich_text_box.clone().set_x_position(self.clone().get_rich_text_box_list().get_x_position());
            rich_text_box.clone().set_menu_material_id(self.clone().get_rich_text_box_list().get_menu_material_id());
            rich_text_box.clone().set_component_index(self.clone().get_rich_text_box_list().get_component_index());
            self.clone().get_rich_text_box_list().push_rich_text_box_text_box(rich_text_box);
        }
    }
    fn remove_rich_text_box_text_box
    (
        self:std::sync::Arc<Self>,
        index:usize
    )
    {
        if index < self.clone().get_rich_text_box_list_len()
        {
            self.clone().get_rich_text_box_list().text_boxes.lock().unwrap().remove(index);
        }
        else if index == self.clone().get_rich_text_box_list_len()
        {
            self.clone().get_rich_text_box_list().text_boxes.lock().unwrap().remove(index);
        }
    }
    fn new_rich_text_box_list_text_box
    (
        self:std::sync::Arc<Self>,
        data:Vec<char>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
    )
    {
        self.clone().push_rich_text_box_text_box
        (
            crate::component_materials::materials::text_box::TextBox::new_text_box
            (
                tui_utilities::utilities::component::Component::new_component
                (
                    None, 
                    Some(0), 
                    Some(0), 
                    Some(String::new()), 
                    Some
                    (
                        tui_utilities::utilities::position::Position::new_position
                        (
                            Some(0), 
                            Some(0)
                        )
                    ), 
                    Some
                    (
                        tui_utilities::utilities::bounds::Bounds::new_bounds
                        (
                            0, 
                            1
                        )
                    )
                ),
                Some(data),
                Some
                (
                    tui_utilities::utilities::indices::Indices::new_indices
                    (
                        Some(0), 
                        Some(0)
                    )
                ),
                Some
                (
                    tui_utilities::utilities::listing_bounds::ListingBounds::new_listing_bounds
                    (
                        Some(0),
                        Some(0),
                        None,
                        None
                    )
                ),
                Some
                (
                    crate::component_materials::materials_utilities::text_buffer::TextBuffer::new_text_buffer
                    (
                        Some(Vec::new()), 
                        None, 
                        tui_utilities::utilities::component::Component::new_component
                        (
                            None, 
                            None, 
                            None, 
                            None, 
                            None, 
                            None
                        )
                    )
                ),
                &cursor
            )
        );
    }
    fn rich_text_box_list_current_text_box_len
    (
        self:std::sync::Arc<Self>,
    ) -> usize
    {
        let highlighted_rich_text_box = self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap()].clone();
        highlighted_rich_text_box.get_text_box_characters_len()
    }
    fn rich_text_box_list_text_box_len
    (
        self:std::sync::Arc<Self>,
        index:usize
    ) -> Option<usize>
    {
        if index < self.clone().get_rich_text_box_list_len()
        {
            let highlighted_rich_text_box = self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[index].clone();
            let temp = highlighted_rich_text_box.characters.lock().unwrap().len();
            Some(temp)
        }
        else
        {
            None
        }
    }
    fn get_rich_text_box_current_text_box
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<crate::component_materials::materials::text_box::TextBox>
    {
        self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap()].clone()
    }
    fn get_rich_text_box_list_text_box
    (
        self:std::sync::Arc<Self>,
        index:usize
    ) -> Option<std::sync::Arc<crate::component_materials::materials::text_box::TextBox>>
    {
        if index < self.clone().get_rich_text_box_list_len()
        {
            Some(self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[index].clone())
        }
        else
        {
            None
        }
    }
    fn get_rich_text_box_list_current_text_box_characters<'a>
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<std::sync::Mutex<Vec<char>>>
    {
        self.clone().get_rich_text_box_current_text_box().characters.clone()
    }
    fn get_rich_text_box_list_current_text_box_characters_mut
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<std::sync::Mutex<Vec<char>>>
    {
        self.clone().get_rich_text_box_current_text_box().characters.clone()
    }
    fn get_rich_text_box_list_text_box_characters_mut
    (
        self:std::sync::Arc<Self>,
        index:usize
    ) -> Option<std::sync::Arc<std::sync::Mutex<Vec<char>>>>
    {
        if index < self.clone().get_rich_text_box_list_len()
        {
            Some(self.clone().get_rich_text_box_list_text_box(index).unwrap().characters.clone())
        }
        else
        {
            None
        }
    }
    fn rich_text_box_list_text_box_between_word
    (
        self:std::sync::Arc<Self>,
    ) -> bool
    {
        self.clone().get_rich_text_box_list_current_text_box_characters().lock().unwrap()[self.clone().get_rich_text_box_list().get_horizontal_indices_index().unwrap()] != ' '
    }
    fn get_rich_text_box_list_text_box_front_chars
    (
        self:std::sync::Arc<Self>,
    ) -> Vec<char>
    {
        let mut temp:Vec<char> = Vec::new();
        for text_box_index in 0..self.clone().get_rich_text_box_list_len()
        {
            if text_box_index == self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap()
            {
                for character_index in 0..self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[text_box_index].characters.lock().unwrap().len()
                {
                   if character_index < self.clone().get_rich_text_box_list().get_horizontal_indices_index().unwrap()
                   {
                        temp.push(self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[text_box_index].characters.lock().unwrap()[character_index].clone());
                   }
                }
            }
        }
        return temp;
    }
    fn get_rich_text_box_list_text_box_back_chars
    (
        self:std::sync::Arc<Self>,
    ) -> Vec<char>
    {
        let mut temp:Vec<char> = Vec::new();
        for text_box_index in 0..self.clone().get_rich_text_box_list_len()
        {
            if text_box_index == self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap()
            {
                for character_index in 0..self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[text_box_index].characters.lock().unwrap().len()
                {
                   if character_index >= self.clone().get_rich_text_box_list().get_horizontal_indices_index().unwrap()
                   {
                        temp.push(self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[text_box_index].characters.lock().unwrap()[character_index].clone());
                   }
                }
            }
        }
        return temp;
    }
    fn get_rich_text_box_list_text_box_back_chars_to_limit
    (
        self:std::sync::Arc<Self>,
        limit:usize
    ) -> Vec<char>
    {
        let mut temp:Vec<char> = Vec::new();
        for text_box_index in 0..self.clone().get_rich_text_box_list_len()
        {
            if text_box_index == self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap()
            {
                let end_temp = 
                {
                    if limit < self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[text_box_index].characters.lock().unwrap().len() || limit < self.clone().get_rich_text_box_list().get_horizontal_indices_index().unwrap()
                    {
                        self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[text_box_index].characters.lock().unwrap().len()
                    }
                    else
                    {
                        limit
                    }
                };
                for character_index in 0..end_temp
                {
                   if character_index >= self.clone().get_rich_text_box_list().get_horizontal_indices_index().unwrap()
                   {
                        temp.push(self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[text_box_index].characters.lock().unwrap()[character_index].clone());
                   }
                }
            }
        }
        return temp;
    }
    fn get_rich_text_box_list_text_box_right_empty_space
    (
        self:std::sync::Arc<Self>,
        up:Option<usize>,
        down:Option<usize>
    ) -> usize
    {
        if up.is_some()
        {
            if  self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap().checked_sub(up.unwrap()).is_none() && 
            self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() - up.unwrap()].characters.lock().unwrap().len() == self.clone().get_rich_text_box_list().get_horizontal_bounds_size()
            {return 0 as usize;}
            let range = self.clone().get_rich_text_box_list().get_horizontal_bounds_size()- self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() - up.unwrap()].characters.lock().unwrap().len();
            range
        }
        else if down.is_some()
        {
            if  self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap().checked_sub(down.unwrap()).is_none() && 
            self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() - down.unwrap()].characters.lock().unwrap().len() == self.clone().get_rich_text_box_list().get_horizontal_bounds_size()
            {return 0 as usize;}
            let range = self.clone().get_rich_text_box_list().get_horizontal_bounds_size()- self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() - down.unwrap()].characters.lock().unwrap().len();
            range
        }
        else
        { 
            let range = self.clone().get_rich_text_box_list().get_horizontal_bounds_size()- self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() - up.unwrap()].characters.lock().unwrap().len();
            range
        }
    }
    fn rich_text_box_list_sync_current_text_box
    (
        self:std::sync::Arc<Self>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
    )
    {
        let highlighted_text_box = self.clone().get_rich_text_box_current_text_box();
        let adjusted_component_info = std::sync::Arc::new((*self.clone().get_rich_text_box_list().get_component()).clone());
        adjusted_component_info.clone().set_y_position
        (
            Some
            (
                self.clone().get_rich_text_box_list().get_y_position().unwrap() + 
                self.clone().get_rich_text_box_list().calculate_pointer_bounds_distance
                (
                    &self.clone().get_rich_text_box_list().get_position(),
                    &cursor.clone().get_position()
                ).unwrap().1.unwrap()
            )
        );
        adjusted_component_info.clone().set_vertical_bounds_size(1);
        highlighted_text_box.clone().set_component(&adjusted_component_info);
        self.clone().get_rich_text_box_list().set_listing_bounds_left(highlighted_text_box.clone().get_listing_bounds_left());
        self.clone().get_rich_text_box_list().set_listing_bounds_right(highlighted_text_box.clone().get_listing_bounds_right()); 
        self.clone().get_rich_text_box_list().set_indices(&highlighted_text_box.clone().get_indices())
    }
    fn generate_rich_text_box_position_array
    (
        self:std::sync::Arc<Self>,
    ) -> Vec<std::sync::Arc<tui_utilities::utilities::position::Position>>
    {
        let mut temp:Vec<std::sync::Arc<tui_utilities::utilities::position::Position>> = Vec::new();
        for index in self.clone().get_rich_text_box_list().generate_vertical_iter().unwrap()
        {
            temp.push
            (
                tui_utilities::utilities::position::Position::new_position
                (
                self.clone().get_rich_text_box_list().get_x_position(), 
                Some(self.clone().get_rich_text_box_list().get_y_position().unwrap() + index)
                )
            );
        }
        temp
    }
    fn rich_text_box_list_rich_text_box_list_call_get_text_box_input
    (
        self:std::sync::Arc<Self>,
        value:char
    )
    {
        let highlighted_rich_text_box = self.clone().get_rich_text_box_current_text_box();
        highlighted_rich_text_box.insert_text_box_character(value);
    }
    #[allow(unused_mut)]
    #[allow(unused_assignments)]
    fn get_rich_text_box_list_input
    (
        self:std::sync::Arc<Self>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
    )
    {
        for input in standard_input.clone().clone().get_input_stream()
        {
            if input.is_character()
            {
                self.clone().rich_text_box_list_sync_current_text_box(cursor);
                if self.clone().rich_text_box_list_current_text_box_len() == self.clone().get_rich_text_box_list().get_horizontal_bounds_size()
                {
                    self.clone().rich_text_box_list_rich_text_box_list_call_get_text_box_input(input.get_character().unwrap());
                    //Algorithm control.
                    let mut switch_1 = true;
                    //Word parsing control.
                    let mut switch_2 = true;
                    //Split using word control.
                    let mut switch_3 = false;
                    let mut index:usize = self.clone().rich_text_box_list_current_text_box_len() - 1;
                    let mut list_of_word_start_stop:Vec<usize> = Vec::new();
                    let last_vertical_index = self.clone().get_rich_text_box_list().get_vertical_indices_index();
                    let mut split_text_box:Vec<char> = Vec::new();
                    while switch_1
                    {
                        while switch_2
                        {
                            //Split if text box has a large word.
                            if  list_of_word_start_stop.len() == 2 && 
                                index == 0
                            {
                                let horizontal_bounds_size = self.clone().get_rich_text_box_list().get_horizontal_bounds_size();
                                split_text_box = self.clone().get_rich_text_box_list_current_text_box_characters_mut().lock().unwrap().split_off(horizontal_bounds_size);
                                switch_2 = false;
                                switch_3 = true;
                                break;
                            }
                            //Split at current index if is space and is the size of horizontal bounds. 
                            if  index == self.clone().get_rich_text_box_list().get_horizontal_bounds_size() && 
                                self.clone().get_rich_text_box_list_current_text_box_characters().lock().unwrap()[index] == ' '
                            {
                                let horizontal_bounds_size = self.clone().get_rich_text_box_list().get_horizontal_bounds_size();
                                split_text_box = self.clone().get_rich_text_box_list_current_text_box_characters_mut().lock().unwrap().split_off(horizontal_bounds_size);
                                switch_2 = false;
                                switch_3 = true;
                                break;
                            }
                            //Skip current index if is space.
                            if self.clone().get_rich_text_box_list_current_text_box_characters().lock().unwrap()[index] == ' '
                            {
                                index = index - 1;
                            }
                            else
                            {
                                //Start/One Word
                                if  self.clone().get_rich_text_box_list_current_text_box_characters().lock().unwrap()[index - 1] == ' ' && 
                                    self.clone().get_rich_text_box_list_current_text_box_characters().lock().unwrap()[index] != ' '
                                {
                                    list_of_word_start_stop.insert(0, index);
                                    //Just here for single word so that the boundary check 
                                    //fails and program does not panic.
                                    if self.clone().rich_text_box_list_current_text_box_len() - 1 == index
                                    {
                                        list_of_word_start_stop.insert(0, index); 
                                    }
                                    index = index - 1;
                                }
                                //Within word
                                else if self.clone().get_rich_text_box_list_current_text_box_characters().lock().unwrap()[index - 1] != ' ' && 
                                        self.clone().get_rich_text_box_list_current_text_box_characters().lock().unwrap()[index] != ' '
                                {
                                    if  self.clone().get_rich_text_box_list_current_text_box_characters().lock().unwrap()[index - 1] != ' ' &&
                                        index - 1 == 0
                                    {
                                        list_of_word_start_stop.insert(0, 0);
                                    }
                                    index = index - 1;
                                }
                                //Stop
                                else if self.clone().get_rich_text_box_list_current_text_box_characters().lock().unwrap()[index] != ' ' && 
                                        self.clone().get_rich_text_box_list_current_text_box_characters().lock().unwrap()[index + 1] == ' '
                                {
                                    list_of_word_start_stop.insert(0, index);
                                    index = index - 1;
                                }
                            }
                            //Check to see if the start and stop of a 
                            //word has the bounds within it's range.
                            if  list_of_word_start_stop[0] > 
                                self.clone().get_rich_text_box_list().get_horizontal_bounds_size() - 1 ||
                                list_of_word_start_stop[0] < 
                                self.clone().get_rich_text_box_list().get_horizontal_bounds_size() - 1 &&
                                list_of_word_start_stop[1] < 
                                self.clone().get_rich_text_box_list().get_horizontal_bounds_size() - 1
                            {
                                switch_2 = false;
                                break;
                            }
                        }
                        if switch_3 == false
                        {
                            split_text_box = self.clone().get_rich_text_box_list_current_text_box_characters_mut().lock().unwrap().split_off(list_of_word_start_stop[0]);
                        } 
                        if  self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() + 1 == 
                            self.clone().get_rich_text_box_list_len()
                        {
                            self.clone().new_rich_text_box_list_text_box(split_text_box.clone(), cursor);
                            self.clone().get_rich_text_box_list().set_vertical_indices_index
                            (
                                Some
                                (
                                    self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() + 1
                                )
                            );
                        }
                        else
                        {
                            self.clone().get_rich_text_box_list().set_vertical_indices_index
                            (
                                Some
                                (
                                    self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() + 1
                                )
                            );
                            self.clone().get_rich_text_box_list_current_text_box_characters_mut().lock().unwrap().append(&mut split_text_box);
                        }
                        if  self.clone().rich_text_box_list_current_text_box_len() < 
                            self.clone().get_rich_text_box_list().get_horizontal_bounds_size()
                        {
                            switch_1 = false;
                            break;
                        }
                        else
                        {
                            switch_2 = true;
                            switch_3 = false;
                        }
                    }
                    self.clone().get_rich_text_box_list().set_vertical_indices_index(last_vertical_index);
                }
                else
                {
                    self.clone().rich_text_box_list_rich_text_box_list_call_get_text_box_input(input.get_character().unwrap());
                }
                self.clone().rich_text_box_list_sync_current_text_box(cursor);
                break;
            }
        }
    }
    fn rich_text_box_list_rich_text_box_list_call_eject_text_box_character
    (
        self:std::sync::Arc<Self>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
    )
    {
        let highlighted_rich_text_box = self.clone().get_rich_text_box_current_text_box();
        highlighted_rich_text_box.text_box_eject_character(standard_input, cursor)
    }
    fn calculate_rich_text_box_list_word_sizes
    (
        self:std::sync::Arc<Self>,
        attempted_word_count:usize,
        index:usize
    ) -> Option<(usize, usize)>
    {
        if index >= self.clone().get_rich_text_box_list().get_vertical_bounds_size()
        {
            return None
        }
        let (start, mut end_plus_space, mut count):(usize, usize, usize) = (0, 0, 0);
        for index in 0..self.clone().rich_text_box_list_text_box_len(index).unwrap()
        {
            //Stop
            if (self.clone().get_rich_text_box_list_current_text_box_characters().lock().unwrap()[index] != ' ' && 
                self.clone().get_rich_text_box_list_current_text_box_characters().lock().unwrap()[index + 1] == ' ') ||
               (self.clone().get_rich_text_box_list_current_text_box_characters().lock().unwrap()[index] != ' ' &&
                self.clone().rich_text_box_list_current_text_box_len() - 1 == index)
            {
                end_plus_space = index + 1;
                count = count + 1;
            }
            else if self.clone().get_rich_text_box_list_current_text_box_characters().lock().unwrap()[index] != ' ' &&
                    self.clone().rich_text_box_list_current_text_box_len() - 1 == index
            {
                end_plus_space = index;
                break;
            }
            else if self.clone().get_rich_text_box_list_current_text_box_characters().lock().unwrap()[index] == ' ' &&
                    self.clone().rich_text_box_list_current_text_box_len() - 1 == index
            {
                end_plus_space = index;
                break;
            }
            if count == attempted_word_count
            {
                break;
            }
        }
        Some((end_plus_space - start + 1, end_plus_space))
    }
    #[allow(unused_mut)]
    #[allow(unused_assignments)]
    fn eject_rich_text_box_list_character
    (
        self:std::sync::Arc<Self>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
    )
    {
        for input in standard_input.clone().get_input_stream()
        {
            if let termion::event::Event::Key(termion::event::Key::Backspace) = input
            {
                self.clone().rich_text_box_list_sync_current_text_box(cursor);
                self.clone().rich_text_box_list_rich_text_box_list_call_eject_text_box_character(standard_input, cursor);
                let last_vertical_index = self.clone().get_rich_text_box_list().get_vertical_indices_index();
                let mut switch_1 = true;
                while switch_1
                {
                    if  self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() == 
                        self.clone().get_rich_text_box_list_len()
                    {
                        switch_1 = false;
                        break;
                    }
                    let word_size_data = 
                    self.clone().calculate_rich_text_box_list_word_sizes
                    (
                        1,
                        self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() + 1
                    );
                    if word_size_data.is_some()
                    {
                        let (word_size, word_end_index) = word_size_data.unwrap();
                        if  word_size == 0 
                        {
                            //Check if the next text box has all characters or all spaces. Then continue the algorithm for the basic check.
                            let split_point = self.clone().calculate_rich_text_box_list_word_sizes(1, self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap()).unwrap().0;
                            let mut split_text_box:Vec<char> = self.clone().get_rich_text_box_list_text_box_characters_mut(self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() + 1).unwrap().lock().unwrap().split_off(split_point);
                            let mut next_text_box = self.clone().get_rich_text_box_list_text_box_characters_mut(self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() + 1).unwrap().clone();
                            self.clone().get_rich_text_box_list_current_text_box_characters_mut().lock().unwrap().append(&mut next_text_box.lock().unwrap());
                            self.clone().get_rich_text_box_list_text_box_characters_mut(self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() + 1).unwrap().lock().unwrap().clear();
                            self.clone().get_rich_text_box_list_text_box_characters_mut(self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() + 1).unwrap().lock().unwrap().append(&mut split_text_box);
                        }
                        else if self.clone().rich_text_box_list_current_text_box_len() - self.clone().get_rich_text_box_list().get_horizontal_bounds_size()== word_size
                        {
                            let mut split_text_box:Vec<char> = self.clone().get_rich_text_box_list_text_box_characters_mut(self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() + 1).unwrap().lock().unwrap().split_off(word_end_index);
                            let mut next_text_box = self.clone().get_rich_text_box_list_text_box_characters_mut(self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() + 1).unwrap().clone();
                            self.clone().get_rich_text_box_list_current_text_box_characters_mut().lock().unwrap().append(&mut next_text_box.lock().unwrap());
                            self.clone().get_rich_text_box_list_text_box_characters_mut(self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() + 1).unwrap().lock().unwrap().clear();
                            self.clone().get_rich_text_box_list_text_box_characters_mut(self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() + 1).unwrap().lock().unwrap().append(&mut split_text_box);
                        }
                        self.clone().get_rich_text_box_list().set_vertical_indices_index(Some(self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() + 1));
                    }
                    else
                    {
                        switch_1 = false;
                        break;
                    }
                }
                self.clone().get_rich_text_box_list().set_vertical_indices_index(last_vertical_index);
                self.clone().rich_text_box_list_sync_current_text_box(cursor);
                break;
            }
        }
    }
    fn draw_rich_text_box_list
    (
        self:std::sync::Arc<Self>,
        standard_output:&std::sync::Arc<tui_utilities::utilities_materials::standard_output::StandardOutput>,
    )
    {
        let listing_bounds = std::sync::Arc::new((*self.clone().get_rich_text_box_list().get_listing_bounds()).clone());
        let position_array = self.clone().generate_rich_text_box_position_array();
        let mut count:usize = 0;
        for rich_text_box_index in 0..self.clone().get_rich_text_box_list_len()
        {
            let old_listing_bounds = self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[rich_text_box_index].clone().listing_bounds.lock().unwrap().clone();
            *self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[rich_text_box_index].clone().listing_bounds.lock().unwrap() = Some(listing_bounds.clone());
            self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[rich_text_box_index].clone().set_position(&position_array[count].clone());
            self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[rich_text_box_index].clone().draw_text_box(standard_output);
            *self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[rich_text_box_index].clone().listing_bounds.lock().unwrap() = old_listing_bounds;
            count+=1;
        }
    }
    fn rich_text_box_list_call_move_text_box_left
    (
        self:std::sync::Arc<Self>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
    )
    {
        let highlighted_rich_text_box = self.clone().get_rich_text_box_current_text_box();
        highlighted_rich_text_box.move_text_box_left(cursor, standard_input)
    }
    fn move_rich_text_box_list_left
    (
        self:std::sync::Arc<Self>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
    )
    {
        for input in standard_input.clone().get_input_stream()
        {
            if let termion::event::Event::Key(termion::event::Key::Left) = input
            {
                self.clone().rich_text_box_list_sync_current_text_box(cursor);
                if self.clone().get_rich_text_box_list().pointer_on_bounds_left(&self.clone().get_rich_text_box_list().get_position(), &cursor.clone().get_position())
                {
                    //Through a series of checks to determine if the cursor should got to the next text box down.
                    if self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() == 0 {return;}
                    self.clone().move_rich_text_box_list_up(cursor, standard_input);
                }
                self.clone().rich_text_box_list_call_move_text_box_left(cursor, standard_input);
                self.clone().rich_text_box_list_sync_current_text_box(cursor);
            }
        }
    }
    fn rich_text_box_list_call_move_text_box_right
    (
        self:std::sync::Arc<Self>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
    )
    {
        let highlighted_rich_text_box = self.clone().get_rich_text_box_current_text_box();
        highlighted_rich_text_box.move_text_box_right(cursor, standard_input)
    }
    fn move_rich_text_box_list_right
    (
        self:std::sync::Arc<Self>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
    )
    {
        for input in standard_input.clone().get_input_stream()
        {
            if let termion::event::Event::Key(termion::event::Key::Right) = input
            {    
                self.clone().rich_text_box_list_sync_current_text_box(cursor);
                if self.clone().get_rich_text_box_list().pointer_on_bounds_right(&self.clone().get_rich_text_box_list().get_position(), &cursor.clone().get_position())
                {
                    //Through a series of checks to determine if the cursor should got to the next text box down.
                    if self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() == self.clone().get_rich_text_box_list_len() - 1{return;}
                    self.clone().move_rich_text_box_list_down(cursor, standard_input);
                }
                self.clone().rich_text_box_list_call_move_text_box_right(cursor, standard_input);
                self.clone().rich_text_box_list_sync_current_text_box(cursor);
            }
        }
    }
    fn move_rich_text_box_list_up
    (
        self:std::sync::Arc<Self>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
    )
    {
        for input in standard_input.clone().get_input_stream()
        {
            if let termion::event::Event::Key(termion::event::Key::Up) = input
            {    
                self.clone().rich_text_box_list_sync_current_text_box(cursor);
                cursor.clone().set_component(&self.clone().get_rich_text_box_list().get_component());
                cursor.clone().clear_menu();
                let new_position = std::sync::Arc::new((*cursor.clone().get_position()).clone());
                new_position.clone().move_position_up(1);
                cursor.clone().move_cursor(&new_position, None);
                self.clone().get_rich_text_box_list().move_listing_bounds_up(1);
                cursor.clone().update_cursor_index(self.clone().get_rich_text_box_list());
                self.clone().rich_text_box_list_sync_current_text_box(cursor);
                break;
            }
        }
    }
    fn move_rich_text_box_list_down
    (
        self:std::sync::Arc<Self>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
    )
    {
        for input in standard_input.clone().get_input_stream()
        {
            if let termion::event::Event::Key(termion::event::Key::Down) = input
            {    
                self.clone().rich_text_box_list_sync_current_text_box(cursor);
                cursor.clone().set_component(&self.clone().get_rich_text_box_list().get_component());
                cursor.clone().clear_menu();
                let new_position = std::sync::Arc::new((*cursor.clone().get_position()).clone());
                new_position.clone().move_position_down(1);
                cursor.clone().move_cursor(&new_position, None);
                self.clone().get_rich_text_box_list().move_listing_bounds_down(1, self.clone().get_rich_text_box_list_len());
                cursor.clone().update_cursor_index(self.clone().get_rich_text_box_list());
                self.clone().rich_text_box_list_sync_current_text_box(cursor);
                break;
            }
        }
    }
    fn get_rich_text_box_list_enter
    (
        self:std::sync::Arc<Self>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
    )
    {
        let temp:Vec<char> = self.clone().get_rich_text_box_list_text_box_back_chars();
        self.clone().new_rich_text_box_list_text_box(temp, cursor);
        self.clone().move_rich_text_box_list_down(cursor, standard_input);
    }
    fn get_rich_text_box_list_backspace
    (
        self:std::sync::Arc<Self>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>,
    )
    {
        for input in standard_input.clone().get_input_stream()
        {
            if let termion::event::Event::Key(termion::event::Key::Backspace) = input
            {
                if self.clone().get_rich_text_box_list().get_horizontal_indices_index().unwrap() == 0
                {
                    if self.clone().get_rich_text_box_list_text_box_right_empty_space(Some(1), None) == 0 {return;}
                    let mut back_characters = self.clone().get_rich_text_box_list_text_box_back_chars_to_limit(self.clone().get_rich_text_box_list_text_box_right_empty_space(Some(1), None));
                    let text_box_above_index = self.clone().get_rich_text_box_list().get_vertical_indices_index().unwrap() - 1;
                    self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[text_box_above_index].characters.lock().unwrap().append(&mut back_characters);
                }
                else
                {
                    self.clone().eject_rich_text_box_list_character(standard_input, cursor)
                }
            }
        }
    }
    fn get_data
    (
        self:std::sync::Arc<Self>,
    ) -> Option<Vec<String>>
    {
        let mut temp:Vec<String> = Vec::new();
        for index in 0..self.clone().get_rich_text_box_list_len()
        {
            temp.push(String::from_iter(self.clone().get_rich_text_box_list().text_boxes.lock().unwrap()[index].characters.lock().unwrap().iter()));
        }
        Some(temp)
    }
}
impl RichTextBoxListOperations for crate::component_materials::materials::rich_text_box_list::RichTextBoxList
{
    fn get_rich_text_box_list
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<crate::component_materials::materials::rich_text_box_list::RichTextBoxList> 
    {
        self.clone()    
    }
    fn any_rich_text_box_list
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::component_operations::ComponentOperations for crate::component_materials::materials::rich_text_box_list::RichTextBoxList
{
    fn get_component
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<tui_utilities::utilities::component::Component> 
    {
        self.clone().component.lock().unwrap().as_ref().unwrap().clone()
    }
    fn any_component
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::component_save_operations::ComponentSaveOperations for crate::component_materials::materials::rich_text_box_list::RichTextBoxList
{
    fn save
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::component_save::ComponentSave> 
    {
        tui_utilities::utilities::component_save::ComponentSave::new_component_save
        (
            self.clone().get_menu_material_id().unwrap(), 
            self.clone().get_material_id().unwrap(), 
            self.clone().get_type_name().unwrap(), 
            self.clone().get_data()
        )    
    }
    fn update
    (
        self:std::sync::Arc<Self>,
        component_save:&std::sync::Arc<tui_utilities::utilities::component_save::ComponentSave>,
        cursor:Option<&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>>
    ) 
    {
        for index in 0..component_save.clone().get_data_len()
        {
            self.clone().push_rich_text_box_text_box
            (
                {
                    let component = 
                    tui_utilities::utilities::component::Component::new_component
                    (
                        None, 
                        None, 
                        None, 
                        None, 
                        Some
                        (
                            tui_utilities::utilities::position::Position::new_position(Some(0), Some(0))
                        ), 
                        Some
                        (
                            tui_utilities::utilities::bounds::Bounds::new_bounds(0, self.clone().get_vertical_bounds_size())
                        )
                    );
                    crate::component_materials::materials::text_box::TextBox::new_text_box
                    (
                        component.clone(), 
                        Some(component_save.clone().get_data(index).unwrap().chars().collect::<Vec<char>>()), 
                        Some
                        (
                            tui_utilities::utilities::indices::Indices::new_indices
                            (
                                Some(0), 
                                Some(0)
                            )
                        ), 
                        Some
                        (
                            tui_utilities::utilities::listing_bounds::ListingBounds::new_listing_bounds
                            (
                                Some(0),
                                Some(0),
                                None,
                                None
                            )
                        ), 
                        Some
                        (
                            crate::component_materials::materials_utilities::text_buffer::TextBuffer::new_text_buffer
                            (
                                None, 
                                None, 
                                component.clone()
                            )
                        ), 
                        cursor.unwrap()
                    )
                }
            )
        }
    }
}
impl tui_utilities::utilities_operations::position_operations::PositionOperations for crate::component_materials::materials::rich_text_box_list::RichTextBoxList
{
    fn get_position
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<tui_utilities::utilities::position::Position> 
    {
        self.clone().get_component().get_position()    
    }
    fn any_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::bounds_operations::BoundsOperations for crate::component_materials::materials::rich_text_box_list::RichTextBoxList
{
    fn get_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<tui_utilities::utilities::bounds::Bounds> 
    {
        self.clone().get_component().get_bounds()    
    }
    fn any_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::indices_operations::IndicesOperations for crate::component_materials::materials::rich_text_box_list::RichTextBoxList
{
    fn get_indices
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<tui_utilities::utilities::indices::Indices> 
    {
        self.clone().indices.lock().unwrap().as_ref().unwrap().clone()    
    }
    fn any_indices
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::listing_bounds_operations::ListingBoundsOperations for crate::component_materials::materials::rich_text_box_list::RichTextBoxList
{
    fn get_listing_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<tui_utilities::utilities::listing_bounds::ListingBounds> 
    {
        self.clone().listing_bounds.lock().unwrap().as_ref().unwrap().clone()    
    }
    fn any_list_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}