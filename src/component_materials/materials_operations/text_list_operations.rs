use tui_utilities::utilities_operations::bounds_operations::BoundsOperations;
use tui_utilities::utilities_operations::key_operations::KeyOperations;
use tui_utilities::utilities_operations::menu_operations::MenuOperations;
use tui_utilities::utilities_operations::{component_operations::ComponentOperations, listing_bounds_operations::ListingBoundsOperations, indices_operations::IndicesOperations, position_operations::PositionOperations};

use tui_utilities::utilities_materials_operations::{cursor_operations::CursorOperations, standard_input_operations::StandardInputOperations};

use crate::component_materials::materials_operations::label_operations::LabelOperations;
pub trait TextListOperations:Send + Sync + 'static
{
    fn get_text_list
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::component_materials::materials::text_list::TextList>;
    fn any_text_list
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>;
    fn set_text_list
    (
        self:std::sync::Arc<Self>,
        text_list:&std::sync::Arc<crate::component_materials::materials::text_list::TextList>
    )
    {
        *self.clone().get_text_list().list.lock().unwrap() = text_list.list.lock().unwrap().clone();
        self.clone().get_text_list().set_component(&text_list.clone().get_component());
        self.clone().get_text_list().set_listing_bounds(text_list.clone().get_listing_bounds());
        self.clone().get_text_list().set_indices(&text_list.clone().get_indices());
    }
    fn get_text_list_len
    (
        self:std::sync::Arc<Self>
    ) -> usize
    {
        self.clone().get_text_list().list.lock().unwrap().len()
    }
    fn push_label
    (
        self:std::sync::Arc<Self>,
        entry:std::sync::Arc<crate::component_materials::materials::label::Label>,
    )
    {
        self.clone().get_text_list().list.lock().unwrap().push(entry)
    }
    fn insert_label
    (
        self:std::sync::Arc<Self>, 
        index:usize,
        entry:std::sync::Arc<crate::component_materials::materials::label::Label>,
    )
    {
        if index < self.clone().get_text_list_len()
        {
            entry.clone().set_x_position(self.clone().get_text_list().get_x_position());
            self.clone().get_text_list().list.lock().unwrap().insert(index, entry)
        }
        else if index == self.clone().get_text_list_len()
        {
            entry.clone().set_x_position(self.clone().get_text_list().get_x_position());
            self.clone().push_label(entry);
        }
    }
    fn remove_label
    (
        self:std::sync::Arc<Self>, 
        index:usize
    )
    {
        if index < self.clone().get_text_list_len()
        {
            self.clone().get_text_list().list.lock().unwrap().remove(index);
        }
        else if index == self.clone().get_text_list_len()
        {
            self.clone().get_text_list().list.lock().unwrap().remove(index);
        }
    }
    fn text_list_get_label
    (
        self:std::sync::Arc<Self>,
        index:usize,
    ) -> std::sync::Arc<crate::component_materials::materials::label::Label>
    {
        self.clone().get_text_list().list.lock().unwrap()[index].clone()
    }
    fn draw_text_list
    (
        self:std::sync::Arc<Self>, 
        standard_output:&std::sync::Arc<tui_utilities::utilities_materials::standard_output::StandardOutput>
    )
    {
        for (index, entry) in self.clone().get_text_list().list.lock().unwrap().iter().enumerate()
        {
            if index <= self.clone().get_text_list().get_listing_bounds_up().unwrap() && index >= self.clone().get_text_list().get_listing_bounds_down().unwrap()
            {
                entry.clone().set_x_position
                (
                    Some
                    (
                        self.clone().get_text_list().get_y_position().unwrap() + (index - self.clone().get_text_list().get_listing_bounds_up().unwrap())
                    )
                );
                entry.clone().draw_label_self(standard_output);
                entry.clone().set_x_position(None);
            }
        }
    }
    fn move_text_list_up
    (
        self:std::sync::Arc<Self>, 
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>
    )
    {
        for input in standard_input.clone().get_input_stream()
        {
            if input.is_up()
            {
                cursor.clone().set_component(&self.clone().get_text_list().get_component());
                cursor.clone().clear_menu();
                let new_position = cursor.clone().get_position();
                new_position.clone().move_position_up(1);
                cursor.clone().move_cursor(&new_position, None);
                if  cursor.clone().get_component().pointer_on_bounds_top
                    (
                        &self.clone().get_text_list().get_position(),
                        &cursor.clone().get_position()
                    )
                {
                    self.clone().get_text_list().move_listing_bounds_up(1);
                }
                cursor.clone().update_cursor_index
                (
                    self.clone().get_text_list()
                );
            }
        }
    }
    fn move_text_list_down
    (
        self:std::sync::Arc<Self>, 
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>
    )
    {
        for input in standard_input.clone().get_input_stream()
        {
            if input.is_down()
            {
                cursor.clone().set_component(&self.clone().get_text_list().get_component());
                cursor.clone().clear_component();
                let new_position = cursor.clone().get_position();
                new_position.clone().move_position_down(1);
                cursor.clone().move_cursor(&new_position, None);
                if  cursor.clone().get_component().pointer_on_bounds_bottom
                    (
                        &self.clone().get_text_list().get_position(),
                        &cursor.clone().get_position()
                    )
                {
                    self.clone().get_text_list().move_listing_bounds_down(1, self.clone().get_text_list_len());
                }
                cursor.clone().update_cursor_index
                (
                    self.clone().get_text_list()
                );
            }
        }
    }
    fn get_data
    (
        self:std::sync::Arc<Self>,
    ) -> Option<Vec<String>>
    {
        let mut temp:Vec<String> = Vec::new();
        for index in 0..self.clone().get_text_list_len()
        {
            temp.push(self.clone().text_list_get_label(index).get_label_string().unwrap());
        }
        Some(temp)
    }
}
impl crate::component_materials::materials_operations::text_list_operations::TextListOperations for crate::component_materials::materials::text_list::TextList
{
    fn get_text_list
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::component_materials::materials::text_list::TextList> 
    {
        self.clone()    
    }
    fn any_text_list
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::component_operations::ComponentOperations for crate::component_materials::materials::text_list::TextList
{
    fn get_component
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::component::Component> 
    {
        self.component.clone()    
    }
    fn any_component
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()
    }
}
impl tui_utilities::utilities_operations::position_operations::PositionOperations for crate::component_materials::materials::text_list::TextList
{
    fn get_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::position::Position> 
    {
        self.get_component().get_position()
    }
    fn any_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()
    }
}
impl tui_utilities::utilities_operations::bounds_operations::BoundsOperations for crate::component_materials::materials::text_list::TextList
{
    fn get_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<tui_utilities::utilities::bounds::Bounds> 
    {
        self.get_component().get_bounds()    
    }
    fn any_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::listing_bounds_operations::ListingBoundsOperations for crate::component_materials::materials::text_list::TextList
{
    fn get_listing_bounds
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::listing_bounds::ListingBounds> 
    {
        self.clone().get_text_list().listing_bounds.clone()    
    }
    fn any_list_bounds
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::indices_operations::IndicesOperations for crate::component_materials::materials::text_list::TextList
{
    fn get_indices
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<tui_utilities::utilities::indices::Indices> 
    {
        self.indices.clone()    
    }
    fn any_indices
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::component_save_operations::ComponentSaveOperations for crate::component_materials::materials::text_list::TextList
{
    fn save
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::component_save::ComponentSave> 
    {
        tui_utilities::utilities::component_save::ComponentSave::new_component_save
        (
            self.clone().get_menu_material_id().unwrap(), 
            self.clone().get_material_id().unwrap(), 
            self.clone().get_type_name().unwrap(), 
            self.clone().get_data()
        ) 
    }
    fn update
    (
        self:std::sync::Arc<Self>,
        component_save:&std::sync::Arc<tui_utilities::utilities::component_save::ComponentSave>,
        _cursor:Option<&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>>
    ) 
    {
        for index in 0..component_save.clone().get_data_len()
        {
            self.clone().push_label
            (
                crate::component_materials::materials::label::Label::new_label
                (
                    component_save.get_data(index), 
                    Some
                    (
                        tui_utilities::utilities::component::Component::new_component
                        (
                            None, 
                            None, 
                            None, 
                            None, 
                            Some
                            (
                                tui_utilities::utilities::position::Position::new_position(Some(0), Some(0))
                            ), 
                            Some
                            (
                                tui_utilities::utilities::bounds::Bounds::new_bounds
                                (
                                    0, 
                                    self.clone().get_text_list().get_vertical_bounds_size()
                                )
                            )
                        )
                    )
                )
            )
        }
    }
}