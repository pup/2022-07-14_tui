use crate::component_materials::materials_operations::label_operations::LabelOperations;
use tui_utilities::{utilities_operations::{component_operations::ComponentOperations, key_operations::KeyOperations, bounds_operations::BoundsOperations, position_operations::PositionOperations}, utilities_materials_operations::standard_input_operations::StandardInputOperations};
pub trait ButtonOperations
{
    fn get_button
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::component_materials::materials::button::Button>;
    fn any_button
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>;
    fn set_button
    (
        self:std::sync::Arc<Self>,
        button:&std::sync::Arc<crate::component_materials::materials::button::Button>
    )
    {
        self.clone().get_button().set_label(&button.clone().get_label());
    }
    fn on_enter_button
    (
        self:std::sync::Arc<Self>,
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>
    ) -> bool
    {
        let mut temp = false;
        for input in standard_input.clone().get_input_stream()
        {
            if input.is_save()
            {
                temp = true;
                break;
            }
        }
        return temp;
    }
    fn on_click_button
    (
        self:std::sync::Arc<Self>, 
        standard_input:&std::sync::Arc<tui_utilities::utilities_materials::standard_input::StandardInput>
    ) -> bool
    {
        let mut temp = false;
        for input in standard_input.clone().get_input_stream()
        {
            if let termion::event::Event::Mouse(termion::event::MouseEvent::Press(termion::event::MouseButton::Left, x, y)) = input
            {
                let position = 
                tui_utilities::utilities::position::Position::new_position
                (
                    Some
                    (
                        x.clone() as usize
                    ), 
                    Some
                    (
                        y.clone() as usize
                    )
                );
                if !self.clone().get_button().pointer_in_bounds(&self.clone().get_button().get_position(), &position) 
                {
                    continue;
                }
                temp = true;
                break;
            }
        }
        return temp;
    }
}
impl ButtonOperations for crate::component_materials::materials::button::Button
{
    fn get_button
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::component_materials::materials::button::Button> 
    {
        self.clone()
    }
    fn any_button
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl crate::component_materials::materials_operations::label_operations::LabelOperations for crate::component_materials::materials::button::Button
{
    fn get_label
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::component_materials::materials::label::Label> 
    {
        self.text.lock().unwrap().as_ref().unwrap().clone()    
    }
    fn any_label
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::component_operations::ComponentOperations for crate::component_materials::materials::button::Button
{
    fn get_component
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::component::Component> 
    {
        self.get_label().get_component()    
    }
    fn any_component
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl thread_manager::materials_operations::MaterialsOperations for crate::component_materials::materials::button::Button
{
    fn get_materials
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<thread_manager::materials::Materials> 
    {
        self.material.clone()    
    }
    fn any_materials
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::component_save_operations::ComponentSaveOperations for crate::component_materials::materials::button::Button
{
    fn save
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::component_save::ComponentSave> 
    {
        tui_utilities::utilities::component_save::ComponentSave::new_component_save
        (
            self.clone().get_menu_material_id().unwrap(), 
            self.clone().get_material_id().unwrap(), 
            self.clone().get_type_name().unwrap(), 
            self.clone().get_data()
        )
    }
    fn update
    (
        self:std::sync::Arc<Self>,
        component_save:&std::sync::Arc<tui_utilities::utilities::component_save::ComponentSave>,
        _cursor:Option<&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>>
    ) 
    {
        self.set_label_string(component_save.get_data(0));
    }
}
impl tui_utilities::utilities_operations::position_operations::PositionOperations for crate::component_materials::materials::button::Button
{
    fn get_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::position::Position> 
    {
        self.get_component().get_position()    
    }
    fn any_position
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::bounds_operations::BoundsOperations for crate::component_materials::materials::button::Button
{
    fn get_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<tui_utilities::utilities::bounds::Bounds> 
    {
        self.get_component().get_bounds()    
    }
    fn any_bounds
    (
        self:std::sync::Arc<Self>,
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}