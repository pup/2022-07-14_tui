use tui_utilities::utilities_operations::{timer_operations::TimerOperations, component_operations::ComponentOperations};

pub trait TextBufferOperations:Send + Sync + 'static
{
    fn get_text_buffer
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::component_materials::materials_utilities::text_buffer::TextBuffer>;
    fn any_text_buffer
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static>;
    fn set_text_buffer
    (
        self:std::sync::Arc<Self>,
        text_buffer:&std::sync::Arc<crate::component_materials::materials_utilities::text_buffer::TextBuffer>
    )
    {
        *self.clone().get_text_buffer().buffer.lock().unwrap() = text_buffer.buffer.lock().unwrap().clone();
        self.clone().get_text_buffer().set_timer(&text_buffer.clone().get_timer());
        self.clone().get_text_buffer().set_component(&text_buffer.clone().get_component());
        *self.clone().get_text_buffer().is_done.lock().unwrap() = text_buffer.is_done.lock().unwrap().clone();
    }
    fn clear_text_buffer
    (
        self:std::sync::Arc<Self>,
    )
    {

    }
    fn text_buffer_is_some
    (
        self:std::sync::Arc<Self>,
    ) -> bool
    {
        true
    }
    fn get_text_buffer_len
    (
        self:std::sync::Arc<Self>
    ) -> usize
    {
        self.clone().get_text_buffer().buffer.lock().unwrap().len()
    }
    fn get_latest_text_buffer
    (
        self:std::sync::Arc<Self>
    ) -> Vec<char>
    {
        self.clone().get_text_buffer().buffer.lock().unwrap()[self.clone().get_text_buffer_len() - 1].clone()
    }
    //Make functions to get and set the buffer as well as include those functions in the operations file.
    fn push_text_buffer
    (
        self:std::sync::Arc<Self>,
        value:Vec<char>
    )
    {
        self.clone().get_text_buffer().buffer.lock().unwrap().push(value)
    }
    fn insert_text_buffer
    (
        self:std::sync::Arc<Self>,
        index:usize,
        element:Vec<char>
    )
    {
        if index < self.clone().get_text_buffer_len()
        {
            self.clone().get_text_buffer().buffer.lock().unwrap().insert(index, element)
        }
        else if index == self.clone().get_text_buffer_len()
        {
            self.clone().push_text_buffer(element)
        }
    }
    fn remove_text_buffer
    (
        self:std::sync::Arc<Self>,
        index:usize,
    )
    {
        if index < self.clone().get_text_buffer_len()
        {
            self.clone().get_text_buffer().buffer.lock().unwrap().remove(index);
        }
        else if index == self.clone().get_text_buffer_len()
        {
            self.clone().get_text_buffer().buffer.lock().unwrap().remove(self.clone().get_text_buffer_len() - 1);
        }
    }
    fn text_buffer_on_done
    (
        self:std::sync::Arc<Self>, 
        buffer:Vec<char>
    ) -> bool
    {
        if self.clone().get_text_buffer().timer_is_done()
        {
            self.clone().get_text_buffer().cache_text_buffer(buffer);
            return true;
        }
        else
        {
            return false
        }
    }
}
impl crate::component_materials::materials_utilities_operations::text_buffer_operations::TextBufferOperations for crate::component_materials::materials_utilities::text_buffer::TextBuffer
{
    fn get_text_buffer
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<crate::component_materials::materials_utilities::text_buffer::TextBuffer> 
    {
        self.clone()    
    }
    fn any_text_buffer
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()
    }
}
impl tui_utilities::utilities_operations::component_operations::ComponentOperations for crate::component_materials::materials_utilities::text_buffer::TextBuffer
{
    fn get_component
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::component::Component> 
    {
        self.clone().component.clone()    
    }
    fn any_component
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl tui_utilities::utilities_operations::timer_operations::TimerOperations for crate::component_materials::materials_utilities::text_buffer::TextBuffer
{
    fn get_timer
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<tui_utilities::utilities::timer::Timer> 
    {
        self.clone().timer.clone()    
    }
    fn any_timer
    (
        self:std::sync::Arc<Self>
    ) -> std::sync::Arc<dyn std::any::Any + Send + Sync + 'static> 
    {
        self.clone()    
    }
}
impl Clone for crate::component_materials::materials_utilities::text_buffer::TextBuffer
{
    fn clone
    (
        &self
    ) -> crate::component_materials::materials_utilities::text_buffer::TextBuffer 
    {
        crate::component_materials::materials_utilities::text_buffer::TextBuffer 
        {
            buffer:
            std::sync::Mutex::new 
            (
                self.buffer.lock().unwrap().clone()
            ),
            timer:
            std::sync::Arc::new
            (
                (*self.timer).clone()
            ),
            component:
            std::sync::Arc::new 
            (
                (*self.component).clone()
            ),
            is_done:
            std::sync::Mutex::new 
            (
                true
            ),
        }
    }
}