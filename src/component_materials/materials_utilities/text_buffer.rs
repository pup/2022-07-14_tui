pub struct TextBuffer
{
    pub(crate) buffer:std::sync::Mutex<Vec<Vec<char>>>,
    pub(crate) timer:std::sync::Arc<tui_utilities::utilities::timer::Timer>,
    pub(crate) component:std::sync::Arc<tui_utilities::utilities::component::Component>,
    pub(crate) is_done:std::sync::Mutex<bool>
}
impl TextBuffer
{
    pub fn new_text_buffer
    (
        buffer:Option<Vec<Vec<char>>>,
        duration:Option<std::time::Duration>,
        component:std::sync::Arc<tui_utilities::utilities::component::Component>
    ) -> std::sync::Arc<Self>
    {
        std::sync::Arc::new 
        (
            TextBuffer
            {
                buffer:
                {
                    if buffer.is_some()
                    {
                        std::sync::Mutex::new 
                        (
                            buffer.unwrap()
                        )
                    }
                    else
                    {
                        std::sync::Mutex::new 
                        (
                            Vec::new()
                        )
                    }
                },
                timer:
                tui_utilities::utilities::timer::Timer::new_timer
                (
                    {
                        if duration.is_some()
                        {
                            duration
                        }
                        else
                        {
                            Some(std::time::Duration::from_millis(3000))
                        }
                    }
                ),
                component:
                component,
                is_done:
                std::sync::Mutex::new 
                (
                    true
                ),
            }
        )
    }
    pub(crate) fn cache_text_buffer
    (
        self:std::sync::Arc<Self>, 
        buffer:Vec<char>
    )
    {
        self.clone().buffer.lock().unwrap().push(buffer);
    }
}