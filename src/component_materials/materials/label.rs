use tui_utilities::utilities_operations::{component_operations::ComponentOperations};

#[allow(dead_code)]
pub struct Label
{
    pub(crate) material:std::sync::Arc<thread_manager::materials::Materials>,
    pub(crate) text:std::sync::Mutex<Option<String>>,
    pub(crate) component:std::sync::Mutex<Option<std::sync::Arc<tui_utilities::utilities::component::Component>>>,
}
impl Label
{
    
    pub fn new_label
    (
        text:Option<String>, 
        component:Option<std::sync::Arc<tui_utilities::utilities::component::Component>>, 
    ) -> std::sync::Arc<Self>
    {
        if component.is_some()
        {
            component.as_ref().unwrap().clone().set_type_name
            (
                Some
                (
                    Label::get_type_name_label()
                )
            );
        }
        std::sync::Arc::new
        (
            Label
            {
                material:
                thread_manager::materials::Materials::new_material(),
                text:
                std::sync::Mutex::new 
                (
                    text
                ),
                component:
                std::sync::Mutex::new
                (
                    component
                ),
            }
        )
    }
    pub fn get_type_name_label() -> String
    {
        "tui::component_materials::materials::label::Label".to_string()
    }
}