Holds a string. A component structure can get and set the
data of a string. Has functionality to draw it's self with
a state function. It also knows where the the component is.
It will have functionality of to get and set the component
position. It is also aware of the fact that may or may not 
have a string. 