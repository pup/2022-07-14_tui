The text box list structure will have a list
of text box structures. The structure will
have the functionality to navigate through
each text box by incrementing through each
index, keeping track of the highlighted
index. The structure will receive component
meta data from the entity structure that is
prepared by the programmer. The structure
will call upon the highlighted text box
structures functions such as input addition
and deletion, as well as cursor movement. The
structure is aware of the entity acting as a 
component held within the component structure.
