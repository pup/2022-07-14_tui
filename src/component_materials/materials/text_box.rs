use tui_utilities::utilities_operations::{component_operations::ComponentOperations, position_operations::PositionOperations, bounds_operations::BoundsOperations};

use crate::component_materials::materials_utilities_operations::text_buffer_operations::TextBufferOperations;

pub struct TextBox
{
    pub(crate) characters:std::sync::Arc<std::sync::Mutex<Vec<char>>>,
    pub(crate) component:std::sync::Mutex<Option<std::sync::Arc<tui_utilities::utilities::component::Component>>>,
    pub(crate) indices:std::sync::Mutex<Option<std::sync::Arc<tui_utilities::utilities::indices::Indices>>>,
    pub(crate) listing_bounds:std::sync::Mutex<Option<std::sync::Arc<tui_utilities::utilities::listing_bounds::ListingBounds>>>,
    pub(crate) text_buffer:std::sync::Mutex<Option<std::sync::Arc<crate::component_materials::materials_utilities::text_buffer::TextBuffer>>>,
}
impl TextBox
{
    pub fn new_text_box
    (
        component:std::sync::Arc<tui_utilities::utilities::component::Component>,
        characters:Option<Vec<char>>,
        indices:Option<std::sync::Arc<tui_utilities::utilities::indices::Indices>>,
        listing_bounds:Option<std::sync::Arc<tui_utilities::utilities::listing_bounds::ListingBounds>>,
        text_buffer:Option<std::sync::Arc<crate::component_materials::materials_utilities::text_buffer::TextBuffer>>,
        cursor:&std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>,
    ) -> std::sync::Arc<Self>
    {
        component.clone().set_type_name
        (
            Some
            (
                TextBox::get_type_name_self()
            )
        );
        std::sync::Arc::new 
        (
            TextBox 
            { 
                characters:
                {
                    if characters.is_some()
                    {
                        std::sync::Arc::new 
                        (
                            std::sync::Mutex::new 
                            (
                                characters.unwrap()
                            )
                        )
                    }
                    else 
                    {
                        std::sync::Arc::new 
                        (
                            std::sync::Mutex::new 
                            (
                                Vec::new()
                            )
                        )
                    }
                }, 
                component:
                std::sync::Mutex::new
                (
                    Some
                    (
                        component.clone()
                    )
                ),
                indices:
                {
                    if indices.is_some()
                    {
                        std::sync::Mutex::new 
                        (
                            indices
                        )
                    }
                    else 
                    {
                        std::sync::Mutex::new 
                        (
                            tui_utilities::utilities::indices::Indices::calculate_new_indices
                            (
                                &component.clone().get_bounds(), 
                                &component.clone().get_position(), 
                                &{
                                    if listing_bounds.is_some()
                                    {
                                        listing_bounds.as_ref().unwrap().clone()
                                    }
                                    else 
                                    {
                                        tui_utilities::utilities::listing_bounds::ListingBounds::new_listing_bounds
                                        (
                                            Some(0), 
                                            Some(component.clone().get_horizontal_bounds_size()), 
                                            None, 
                                            None
                                        )
                                    }
                                }, 
                                &cursor.clone().get_position()
                            )
                        )
                    }
                },
                listing_bounds:
                std::sync::Mutex::new 
                (
                    {
                        if listing_bounds.clone().is_some()
                        {
                            listing_bounds.clone()
                        }
                        else 
                        {
                            Some
                            (
                                tui_utilities::utilities::listing_bounds::ListingBounds::new_listing_bounds
                                (
                                    Some(0), 
                                    Some(component.clone().get_horizontal_bounds_size()), 
                                    None, 
                                    None
                                )
                            )
                        }
                    }
                ),
                text_buffer:
                {
                    if text_buffer.is_some()
                    {
                        std::sync::Mutex::new 
                        (
                            text_buffer
                        )
                    }
                    else
                    {
                        std::sync::Mutex::new 
                        (
                            Some
                            (
                                crate::component_materials::materials_utilities::text_buffer::TextBuffer::new_text_buffer
                                (
                                    None, 
                                    None,
                                    component.clone()
                                )
                            )
                        )
                    }
                }
            }
        )
    }
    pub fn get_type_name_self
    (
    ) -> String
    {
        "tui::component_materials::materials::text_box::TextBox".to_string()
    }
    pub(crate) fn cache_buffer
    (
        self:&std::sync::Arc<Self>
    )
    {
        if true == self.clone().text_buffer_on_done(self.clone().characters.lock().unwrap().clone()) {}
    }
}
pub fn cache_text_box_to_text_buffer
(
    materials:&Vec<std::sync::Arc<dyn thread_manager::materials_operations::MaterialsOperations>>
)
{
    let text_box = materials[0].clone().any_materials().downcast::<TextBox>().unwrap();
    text_box.clone().text_buffer_on_done(text_box.clone().characters.lock().unwrap().clone());
}