#[allow(unused_imports)]
use tui_utilities::utilities_operations::{listing_bounds_operations::ListingBoundsOperations, indices_operations::IndicesOperations, bounds_operations::BoundsOperations, component_operations::ComponentOperations, position_operations::PositionOperations};

pub struct TextList
{
    pub(crate) list:std::sync::Mutex<Vec<std::sync::Arc<crate::component_materials::materials::label::Label>>>,
    pub(crate) component:std::sync::Arc<tui_utilities::utilities::component::Component>,
    pub(crate) listing_bounds:std::sync::Arc<tui_utilities::utilities::listing_bounds::ListingBounds>,
    pub(crate) indices:std::sync::Arc<tui_utilities::utilities::indices::Indices>,
}
impl TextList
{
    pub fn new_text_list
    (
        component:std::sync::Arc<tui_utilities::utilities::component::Component>,
        list:Option<Vec<std::sync::Arc<crate::component_materials::materials::label::Label>>>,
        listing_bounds:Option<std::sync::Arc<tui_utilities::utilities::listing_bounds::ListingBounds>>,
        indices:Option<std::sync::Arc<tui_utilities::utilities::indices::Indices>>,
        cursor:std::sync::Arc<tui_utilities::utilities_materials::cursor::Cursor>
    ) -> std::sync::Arc<Self>
    {
        component.clone().set_type_name
        (
            Some
            (
                TextList::get_type_name_self()
            )
        );
        let temp_list_bounds =                         
        tui_utilities::utilities::listing_bounds::ListingBounds::new_listing_bounds
        (
            Some(0), 
            Some(component.clone().get_horizontal_bounds_size()), 
            Some(0), 
            Some(component.clone().get_vertical_bounds_size())
        );
        std::sync::Arc::new 
        (
            TextList
            {
                list:
                {
                    if list.is_some()
                    {
                        std::sync::Mutex::new 
                        (
                            list.unwrap()
                        )
                    }
                    else
                    {
                        std::sync::Mutex::new 
                        (
                            Vec::new()
                        )
                    }
                },
                component:component.clone(),
                listing_bounds:
                {
                    if listing_bounds.is_some()
                    {
                        listing_bounds.unwrap()
                    }
                    else
                    {
                        temp_list_bounds.clone()
                    }
                },
                indices:
                {
                    if indices.is_some()
                    {
                        indices.unwrap()
                    }
                    else 
                    {
                        tui_utilities::utilities::indices::Indices::calculate_new_indices
                        (
                            &component.clone().get_bounds(), 
                            &component.clone().get_position(), 
                            &temp_list_bounds.clone(), 
                            &cursor.clone().get_position()
                        ).unwrap()
                    }
                }
            }
        )
    }
    pub fn get_type_name_self() -> String
    {
        "tui::component_materials::materials::text_list::TextList".to_string()
    }
}