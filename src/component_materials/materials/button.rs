use tui_utilities::utilities_operations::{component_operations::ComponentOperations};

pub struct Button
{
    pub(crate) material:std::sync::Arc<thread_manager::materials::Materials>,
    pub(crate) text:std::sync::Mutex<Option<std::sync::Arc<crate::component_materials::materials::label::Label>>>,
}
impl Button
{
    pub fn new_button
    (
        text:Option<String>, 
        component:Option<std::sync::Arc<tui_utilities::utilities::component::Component>>, 
    ) -> std::sync::Arc<Self>
    {
        if component.is_some()
        {
            component.as_ref().unwrap().clone().set_type_name
            (
                Some
                (
                    Button::get_type_name_button()
                )
            );
        }
        std::sync::Arc::new
        (
            Button 
            { 
                material:
                thread_manager::materials::Materials::new_material(),
                text:
                std::sync::Mutex::new 
                (
                    Some
                    (
                        crate::component_materials::materials::label::Label::new_label
                        (
                            text, 
                            component
                        )
                    )
                )
            }
        )
    }
    pub fn get_type_name_button() -> String
    {
        "tui::component_materials::materials::button::Button".to_string()
    }
}