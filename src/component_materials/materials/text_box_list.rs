use tui_utilities::utilities_operations::{component_operations::ComponentOperations, bounds_operations::BoundsOperations};

pub struct TextBoxList
{
    pub(crate) text_boxes:std::sync::Mutex<Vec<std::sync::Arc<crate::component_materials::materials::text_box::TextBox>>>,
    pub(crate) component:std::sync::Mutex<Option<std::sync::Arc<tui_utilities::utilities::component::Component>>>,
    pub(crate) indices:std::sync::Mutex<Option<std::sync::Arc<tui_utilities::utilities::indices::Indices>>>,
    pub(crate) listing_bounds:std::sync::Mutex<Option<std::sync::Arc<tui_utilities::utilities::listing_bounds::ListingBounds>>>,
    pub(crate) largest_text_box:std::sync::Mutex<usize>,
}
impl TextBoxList
{
    pub fn new_text_box_list
    (
        component:std::sync::Arc<tui_utilities::utilities::component::Component>,
        text_boxes:Option<Vec<std::sync::Arc<crate::component_materials::materials::text_box::TextBox>>>,
    ) -> std::sync::Arc<Self>
    {
        component.clone().set_type_name
        (
            Some
            (
                TextBoxList::get_type_name_self()
            )
        );
        std::sync::Arc::new 
        (
            TextBoxList 
            { 
                text_boxes:
                {
                    if text_boxes.is_some()
                    {
                        std::sync::Mutex::new
                        (
                            text_boxes.unwrap()
                        )
                    }
                    else
                    {
                        std::sync::Mutex::new 
                        (
                            Vec::new()
                        )
                    }
                }, 
                component:
                std::sync::Mutex::new 
                (
                    Some
                    (
                        component.clone()
                    )
                ), 
                indices:
                std::sync::Mutex::new 
                (
                    Some
                    (
                        tui_utilities::utilities::indices::Indices::new_indices
                        (
                            Some(0),
                            Some(0)
                        )
                    )
                ), 
                listing_bounds:
                std::sync::Mutex::new 
                (
                    Some
                    (
                        tui_utilities::utilities::listing_bounds::ListingBounds::new_listing_bounds
                        (
                            Some(0), 
                            Some
                            (
                                component.clone().get_horizontal_bounds_size()
                            ), 
                            Some(0), 
                            Some
                            (
                                component.clone().get_vertical_bounds_size()
                            )
                        )
                    )
                ),
                largest_text_box:
                std::sync::Mutex::new 
                (
                    0
                )
            }
        )
    }
    pub fn get_type_name_self
    (
    ) -> String
    {
        "tui::component_materials::materials::text_box_list::TextBoxList".to_string()
    }
}
pub fn cache_buffer
(
    materials:&Vec<std::sync::Arc<dyn thread_manager::materials_operations::MaterialsOperations>>
)
{
    let text_box_list = materials[0].clone().any_materials().downcast::<TextBoxList>().unwrap();
    for text_box in text_box_list.text_boxes.lock().unwrap().iter_mut()
    {
        text_box.cache_buffer()
    }
}